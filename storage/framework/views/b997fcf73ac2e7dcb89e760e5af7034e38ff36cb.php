<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo e(route('admin::index')); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><?php echo config('adminlte.logo_mini'); ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?php echo config('adminlte.logo_lg'); ?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo e(AuthAdmin::user()->getLogoPath()); ?>" class="user-image"
                             alt="<?php echo e(AuthAdmin::user()->name); ?>">
                        <span class="hidden-xs"><?php echo e(AuthAdmin::user()->name); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo e(AuthAdmin::user()->getLogoPath()); ?>" class="img-circle"
                                 alt="<?php echo e(AuthAdmin::user()->name); ?>">

                            <p>
                                <?php echo e(AuthAdmin::user()->name); ?>

                                <small>Member
                                    since <?php echo e(Carbon::parse(AuthAdmin::user()->created_at)->toFormattedDateString()); ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo e(route('admin.logout')); ?>"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"
                                   class="btn btn-default btn-flat">
                                    Logout
                                </a>
                                <form id="logout-form" action="<?php echo e(route('admin.logout')); ?>" method="POST"
                                      style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
