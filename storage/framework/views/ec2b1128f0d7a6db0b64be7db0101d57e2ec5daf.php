<?php

$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : 'Danh sách');
$_listLink = route($resourceRoutesAlias . '.index');
$_createLink = route($resourceRoutesAlias . '.create');
$_mutipleDeleteLink = route($resourceRoutesAlias . '.deletes');

$tableCounter = 0;
$total = 0;
if (count($records) > 0) {
    if ($records[0]->total) {
        foreach ($records as $record) {
            $total += $record->total;
        }
    } else {
        $total = $records->total();
        $tableCounter = ($records->currentPage() - 1) * $records->perPage();
        $tableCounter = $tableCounter > 0 ? $tableCounter : 0;
    }
}
?>

<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render($resourceRoutesAlias); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('page-title', $_pageTitle); ?>


<?php $__env->startSection('page-subtitle', $_pageSubtitle); ?>


<?php $__env->startSection('head-extras'); ?>
    ##parent-placeholder-eabe76b5c33d3d1e8dad24cd5afb8f00710c56db##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo e($_pageSubtitle); ?></h3>
            <!-- Search -->
            <div class="box-tools pull-right">
                <form id="searchForm" class="form" role="form" method="GET" action="<?php echo e($_listLink); ?>">
                    <?php if( isset($resourceSearchExtend)): ?>
                        <?php echo $__env->make($resourceSearchExtend, ['search' => $search, '$createLink' => $_createLink], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php else: ?>
                        <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                            <input type="text" name="search" class="form-control" value="<?php echo e($search); ?>"
                                   placeholder="Search...">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <a href="<?php echo e($_createLink); ?>" class="btn btn-sm btn-primary pull-right toggle-create">
                            <i class="fa fa-plus"></i> <span>Thêm mới</span>
                        </a>
                    <?php endif; ?>
                </form>

            </div>
            <!-- END Search -->

        </div>

        <div class="box-body no-padding">
            <?php if(count($records) > 0): ?>
                <div class="padding-5">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" class="btn btn-sm btn-danger" id="btnDeleteMutiple">

                                <i class="fa fa-close"></i> <span>Xóa bản ghi được chọn</span>

                            </a>
                            <form style="display: none" method="post" action="<?php echo e($_mutipleDeleteLink); ?>"
                                  id="formDeleteMutiple">
                                <?php echo csrf_field(); ?>
                                <div id="id-list">

                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6 text-right">

                            <span class="text-green padding-l-5">Tất cả: <?php echo e($total); ?> bản ghi.</span>&nbsp;

                        </div>
                    </div>
                </div>
                <?php echo $__env->make($resourceAlias.'.table', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>

                <p class="margin-l-5 lead text-green">Không có dữ liệu.</p>

            <?php endif; ?>
        </div>
        <!-- /.box-body -->
        <?php if(count($records) > 0): ?>
            <?php echo $__env->make('common.paginate', ['records' => $records], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

    </div>
    <!-- /.box -->

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footer-extras'); ?>
    <?php echo $__env->make('admin._resources._list-footer-extras', ['sortByParams' => []], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-scripts'); ?>
    <script>

    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>