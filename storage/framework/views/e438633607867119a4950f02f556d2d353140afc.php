<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex, nofollow, noarchive">
<!-- CSRF Token -->
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<link rel="shortcut icon" href="/img/favicons/favicon.ico">
<link rel="icon" href="/img/favicons/favicon.ico" type="image/x-icon" id="favicon">
<link rel="preload" href="/img/favicons/favicon-unread.ico" as="image" id="favicon-unread">

