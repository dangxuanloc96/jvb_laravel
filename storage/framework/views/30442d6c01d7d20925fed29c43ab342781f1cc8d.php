<!--Main Navigation-->
<?php
$notifications = \App\Models\Notification::where('user_id', \Illuminate\Support\Facades\Auth::id())->with('sender:id,avatar')->orderBy('created_at', 'desc')->take(100)->get();
$notificationCount = 0;
foreach ($notifications as $notification) {
    if ($notification->read_at == null)
        $notificationCount++;
}

?>

<header>
    <!-- Navbar -->
    
    <nav id="main-nav" class="navbar fixed-top navbar-light white scrolling-navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="float-left ">
                <a href="#" data-activates="slide-out" class="navbar-toggler button-collapse"><i
                            class="fas fa-bars" style="color: white;margin-top: 5px"></i><span
                            class="sr-only" aria-hidden="true">Menu</span></a>
            </div>
            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="/">
                <strong class="white-text">BBS</strong>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                
                <span class="far fa-address-card"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto custom-scrollbar">
                    <li class="d-none d-xl-block nav-item <?php echo e(\App\Utils::checkRoute(['work_time']) ? 'active': ''); ?>">
                        <a href="<?php echo e(route('work_time')); ?>" class="nav-link waves-effect"><?php echo e(__l('work_time')); ?>

                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo e(\App\Utils::checkRoute(['day_off', 'day_off_approval']) ? 'active': ''); ?>">
                        <a href="<?php echo e(route('day_off')); ?>" class="nav-link waves-effect"><?php echo e(__l('day_off')); ?>

                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo e(\App\Utils::checkRoute(['ask_permission']) ? 'active': ''); ?>">
                        <a href="<?php echo e(route('ask_permission')); ?>"
                           class="nav-link waves-effect"><?php echo e(__l('ask_permission')); ?>

                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo e(\App\Utils::checkRoute(['punish']) ? 'active': ''); ?>">
                        <a href="<?php echo e(route('punish')); ?>"
                           class="nav-link waves-effect"><?php echo e(__l('Punish')); ?>

                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item  <?php echo e(\App\Utils::checkRoute(['report']) ? 'active': ''); ?>">
                        <a href="<?php echo e(route('report')); ?>"
                           class="nav-link waves-effect"><?php echo e(__l('Report')); ?>

                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    
                    
                    
                    
                    
                    <li class="nav-item d-block d-lg-none <?php echo e(\App\Utils::checkRoute(['changePassword']) ? 'active': ''); ?>">
                        <a class="nav-link waves-effect"
                           href="<?php echo e(route('changePassword')); ?>"><?php echo e(__l('change_password')); ?></a>
                    </li>
                    <li class="nav-item d-block d-lg-none <?php echo e(\App\Utils::checkRoute(['logout']) ? 'active': ''); ?>">
                        <a class="nav-link waves-effect"
                           href="<?php echo e(route('logout')); ?>"><?php echo e(__l('logout')); ?>

                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link waves-effect waves-light" id="nav_bar_avatar">
                            <img src="<?php echo e(Auth::user()->avatar); ?>" onerror="this.src='<?php echo e(URL_IMAGE_NO_AVATAR); ?>'"
                                 class="rounded-circle z-depth-0" alt="avatar image">
                        </a>
                    </li>
                    <li class="nav-item dropdown d-none d-sm-block">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false"><?php echo e(Auth::user()->name); ?></a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item d-none d-sm-block"
                               href="<?php echo e(route('profile')); ?>"><?php echo e(__l('Profile')); ?></a>
                            <a class="dropdown-item" href="<?php echo e(route('changePassword')); ?>"><?php echo e(__l('change_password')); ?></a>
                            <div class="dropdown-divider"></div>
                            <?php if(\Illuminate\Support\Facades\Auth::user()->is_remote_checkin == IS_REMOTE_STAFF): ?>
                                <a class="dropdown-item"
                                   href="<?php echo e(route('check_out')); ?>"><?php echo e(__l('checkout')); ?>

                                </a>
                            <?php endif; ?>
                            <a class="dropdown-item"
                               href="<?php echo e(route('logout')); ?>"><?php echo e(__l('logout')); ?>

                            </a>
                        </div>
                    </li>
                </ul>
                <!-- Right -->
                <ul class="navbar-nav mr-2">
                    <li class="nav-item dropdown">
                        <a style="font-size: 22px" class="nav-link position-relative" data-toggle="dropdown" href="#"
                           role="button" aria-expanded="false" id="btnNotification"><i class="bell fas fa-bell"></i></a>
                        <?php if($notifications->isNotEmpty()): ?>
                            <div class="badge position-absolute text-center lblNotifyBagde"
                                 data-count="<?php echo e($notificationCount); ?>"><?php echo e($notificationCount); ?></div>
                            <div class="dropdown-menu dropdown-right z-depth-1" id="notification">
                                <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="dropdown-item notify-read-<?php echo e($notification->is_read); ?>"
                                         onclick="location.href='<?php echo e($notification->data); ?>'">
                                        <div class="notice-img text-center d-flex justify-content-center ">
                                            <img class="rounded-circle"
                                                 src="<?php echo e($notification->sender->avatar ?? JVB_LOGO_URL); ?>"/>
                                        </div>
                                        <div class="notice-content ">
                                            <div class="wrap-text notice-title"
                                                 title="<?php echo e($notification->title); ?>"><?php echo $notification->title; ?></div>
                                            <div class="text-gray wrap-text notice-text"
                                                 title="<?php echo e(strip_tags($notification->content)); ?>"><?php echo strip_tags($notification->content); ?></div>
                                            <div class="text-gray">
                                                <i>
                                                    <span class="notice-icon <?php echo e(NOTIFICATION_LOGO[$notification->logo_id] ?? NOTIFICATION_LOGO[0]); ?>"></span>
                                                    <span class="time-subcribe"
                                                          data-time="<?php echo e($notification->created_at); ?>"
                                                          title="<?php echo e($notification->created_at); ?>"><?php echo e(get_beautiful_time($notification->created_at)); ?></span>
                                                </i>
                                            </div>
                                        </div>

                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                        <?php endif; ?>

                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <!-- Navbar -->
    <div id="notification_template" class="hidden">
        <div class="dropdown-item notify-read-0">
            <div class="notice-img text-center d-flex justify-content-center ">
                <img class="rounded-circle" src="<?php echo e(JVB_LOGO_URL); ?>"/>
            </div>
            <div class="notice-content ">
                <div class="wrap-text notice-title"></div>
                <div class="text-gray wrap-text notice-text"></div>
                <div class="text-gray">
                    <i>
                        <span class="notice-icon"></span>
                        <span class="time-subcribe"
                              data-time=""
                              title="">
                                                </span>
                    </i>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('layouts.partials.frontend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</header>
<!--Main Navigation-->
<?php $__env->startPush('extend-js'); ?>
    <script>
      $(function () {
        window.titleBlink = null;
        var originalTitle = document.title;
        window.blinkNotificationCount = function () {
          if (window.titleBlink == null) {
            var showNotice = false;
            window.titleBlink = setInterval(function () {
              totalNotification = $(".lblNotifyBagde").attr('data-count');
              if (showNotice) {
                document.title = '(' + totalNotification + ') ' + originalTitle;
              } else {
                document.title = originalTitle;
              }
              showNotice ^= true;
            }, 2000);
          }
        }
        var totalNotification = '<?php echo e($notificationCount); ?>' || 0;
        if (totalNotification != 0) {
          blinkNotificationCount();
        }

        $("#btnNotification").click(function () {
          if (window.titleBlink) {
            clearInterval(window.titleBlink);
            window.titleBlink = null;
          }
          $("#favicon").attr("href", "/img/favicons/favicon.ico");
          document.title = originalTitle;

          if ($(".lblNotifyBagde").attr('data-count') == 0) {
            $("#notification .dropdown-item").removeClass('notify-read-0').addClass('notify-read-1');
          } else {
            $.ajax({
              url: '<?php echo e(route('notification_mark_read')); ?>',
              dataType: 'JSON',
              type: 'POST',
              success: function (data) {
                $(".lblNotifyBagde").attr('data-count', 0).text(0).hide();
              }
            });
          }
        });
      })
    </script>
<?php $__env->stopPush(); ?>
