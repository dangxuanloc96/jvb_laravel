<?php
$baseRoute = 'admin::posts';
?>



<?php $__env->startSection('form-content'); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group margin-b-5 margin-t-5<?php echo e($errors->has('user_id') ? ' has-error' : ''); ?>">
                <label for="user_id">Chọn nhân viên</label>
                <?php echo e(Form::select('users_id[]', $users, null, ['id'=> 'user_id', 'class'=>'form-control selectpicker', 'multiple', 'data-live-search'=>'true'])); ?>

            </div>
            <div class="form-group margin-b-5 margin-t-5<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                <label for="title">Tiêu đề *</label>
                <input type="text" class="form-control" name="title" placeholder="Nhập tiêu đề"
                       value="<?php echo old('title'); ?> " required>

                <?php if($errors->has('title')): ?>
                    <div class="help-block">
                        <strong><?php echo e($errors->first('title')); ?></strong>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group margin-b-5 margin-t-5<?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
                <label for="content">Nội dung *</label>
                <input type="text" class="form-control" name="content" placeholder="Nhập nội dung bạn muốn gửi"
                       value="<?php echo old('content'); ?> " required>

                <?php if($errors->has('content')): ?>
                    <div class="help-block">
                        <strong><?php echo e($errors->first('content')); ?></strong>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group margin-b-5 margin-t-5<?php echo e($errors->has('url') ? ' has-error' : ''); ?>">
                <label for="url">Đường dẫn</label>
                <input type="text" class="form-control" name="url" placeholder="https://bbs.hatoq.com/etc..."
                       value="<?php echo old('url'); ?>">

                <?php if($errors->has('url')): ?>
                    <div class="help-block">
                        <strong><?php echo e($errors->first('url')); ?></strong>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-scripts'); ?>
    <link rel="stylesheet" type="text/css"
          href="/css/bootstrap-select.min.css"/>
    <script rel="script" src="/js/bootstrap-select.min.js"></script>

    <script>
        $(function () {
            $("#user_id").selectpicker();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin._resources._simple_form', [
    'breadCrumb'=> 'admin::posts.broadcast',
    'baseRoute'=> $baseRoute,
    'formAction'=> 'admin::posts.sendBroadcast',
    'pageTitle'=> 'Thông báo nhanh',
], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>