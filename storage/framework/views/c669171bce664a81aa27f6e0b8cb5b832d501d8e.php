<?php
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : $resourceTitle);
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Sửa thông tin " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index');
$_createLink = route($resourceRoutesAlias . '.create');

$_updateLink = route($resourceRoutesAlias . '.update', $record->id);
$_printLink = false;
?>


<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render($resourceRoutesAlias.'.edit', $record->id); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title', $_pageTitle); ?>


<?php $__env->startSection('page-subtitle', $_pageSubtitle); ?>


<?php $__env->startSection('head-extras'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST"
                      action="<?php echo e($_updateLink); ?>"
                      enctype="multipart/form-data"<?php echo e($_formFiles === true ? 'enctype="multipart/form-data"' : ''); ?>>
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('PUT')); ?>


                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa thông tin </h3>

                        <div class="box-tools">
                            
                            
                            
                            <a href="<?php echo e($_listLink); ?>" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="<?php echo e($_createLink); ?>" class="btn btn-sm btn-success margin-r-5 margin-l-5 toggle-create">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            <?php if($_printLink): ?>
                                <a href="<?php echo e($_printLink); ?>" target="_blank"
                                   class="btn btn-sm btn-default margin-r-5 margin-l-5">
                                    <i class="fa fa-print"></i> <span>Print</span>
                                </a>
                            <?php endif; ?>
                            <button class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                <i class="fa fa-save"></i> <span>Lưu</span>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="col-md-9">
                            <?php $__currentLoopData = $record->laborCalendarDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="name">Tên người trực nhật </label>
                                        <input type="text" readonly class="form-control" name="name" placeholder="Tên nhóm" value="<?php echo e(\App\Models\User::find($detail->user_id)->name); ?>" required="">

                                    </div>

                                </div>
                                <div class="col-md-6"></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="leader_id">Ngày trực nhật </label>
                                        <input type="text" readonly class="form-control" value="<?php echo e(Carbon::parse($record->labor_date)->format(DATE_FORMAT)); ?>">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group margin-b-5 margin-t-5">
                                            <label for="leader_id">Trạng thái *</label>
                                            <select class="form-control" name="status">
                                                <?php $__currentLoopData = LABOR_STATUS_NAME; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val => $statusName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($val); ?>" <?php echo e($val == $record->status ? 'selected' : ''); ?>><?php echo e($statusName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="description">Lý do chưa hoàn thành</label>
                                        <textarea id="note" class="form-control" name="note" rows="5"
                                                  placeholder="Mô tả">
                                            <?php echo e($record->note); ?>

                                        </textarea>
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <?php if($resourceRoutesAlias != 'admin::day_offs'): ?>

                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with">
                                <div align="center">
                                    <button class="btn btn-info mr-2">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </button>
                                    <a href="<?php echo e($_listLink); ?>" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                <?php endif; ?>
                <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
<?php $__env->stopSection(); ?>


<?php $__env->startSection('footer-extras'); ?>
    <script src="<?php echo e(asset_ver('js/tinymce/tinymce.min.js')); ?>"></script>
    <script !src="">
        tinymce.init({
            selector: 'textarea',
            paste_data_images: true,
            height: '350px',
            width: '99%',
            language: 'vi',
            plugins: [
                "advlist autolink lists charmap preview hr anchor pagebreak",
            ],
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>