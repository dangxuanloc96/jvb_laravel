<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <?php if(isset($config) && $config->lastest_event_image): ?>
        <?php
        $imageUrl = url($config->lastest_event_image);
        ?>
        <meta property="og:image" content="<?php echo e($imageUrl); ?>">
        <meta itemprop="image" content="<?php echo e($imageUrl); ?>">
    <?php endif; ?>

    <?php echo $__env->make('layouts.partials.frontend.meta', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <title><?php echo e(config('app.name', 'BBS')); ?></title>

    <!-- Styles -->
    <link href="<?php echo e(asset_ver('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset_ver('css/mdb.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset_ver('css/style.css')); ?>" rel="stylesheet">
</head>
<body>

<div id="app">
    <?php echo $__env->yieldContent('content'); ?>
</div>

<!-- Scripts -->
<script type="text/javascript" src="<?php echo e(asset_ver('js/jquery-3.3.1.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('js/popper.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('js/mdb.min.js')); ?>"></script>
<?php echo $__env->yieldContent('js-extend'); ?>
</body>
</html>
