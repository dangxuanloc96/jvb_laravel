<?php

?>


<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render('admin::labor_calendar'); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('page-title', 'Lịch trực nhật'); ?>


<?php $__env->startSection('page-subtitle', 'Danh sách trực nhật trong tháng'); ?>


<?php $__env->startSection('head-extras'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css">


<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/interaction/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.js"></script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Edit Form -->
    <div class="box box-info" id="wrap-edit-box">

            <div class="box-header with-border">
                <h3 class="box-title">Danh sách</h3>
                <div class="box-tools">
                    <form class="form" role="form"  style="display: inline-block"
                          action="<?php echo e(route('admin::labor_calendar.index')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo $__env->make('admin.labor_calendar._search_extend', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </form>
                    <a href="<?php echo e(route($resourceRoutesAlias.'.create')); ?>" class="btn btn-sm btn-primary pull-right">
                        <i class="fa fa-plus"></i> <span>Thêm mới</span>
                    </a>
                </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <?php echo $__env->make($resourceAlias.'.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <!-- /.box-body -->

            <div class="box-footer clearfix">
                <!-- Edit Button -->
            </div>
            <!-- /.box-footer -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-extras'); ?>
    <script >
        $(document).ready(function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'interaction' ],
                plugins: [ 'dayGrid' ],
                locale: 'vi',
                header: {
                    left: 'title',
                    // left: 'prev,next today',
                    // center: 'title',
                    // right: 'month,agendaWeek,agendaDay,listWeek'
                    right: null
                },
                defaultDate: '<?php echo e($nowDate); ?>',
                navLinks: true,
                eventLimit: true,
                events: [
                    <?php $__currentLoopData = $laborCalendars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $laborCalendar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    {
                        id: '<?php if($laborCalendar['status'] == 1): ?> Đã hoàn thành <?php elseif($laborCalendar['status'] == 2): ?> Chưa hoàn thành <?php endif; ?>',
                        title: '<?php echo e($laborCalendar['title']); ?>',
                        start: '<?php echo e($laborCalendar['start_date']); ?>',
                        allDay: true,
                        backgroundColor: '<?php if($laborCalendar['status'] == 1): ?> #00ffff <?php elseif($laborCalendar['status'] == 2): ?> #d60c0c <?php else: ?> #999999 <?php endif; ?>',
                        groupId: '<?php echo e($laborCalendar['group_id']); ?>',
                        url: '<?php echo e($laborCalendar['url']); ?>'
                    },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                eventRender: function(info) {
                    $(info.el).tooltip({ title: info.event.id });
                }
            });



            calendar.render();

        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>