<?php
$team = \Auth::user()->team();

$logoUrl = ($team && $team->banner) ? lfm_thumbnail($team->banner) : JVB_LOGO_URL;
$name = $team->name ?? $config->name;
?>

<!-- Sidebar -->
<div class="sidebar fixed sidebar-fixed position-fixed" id="slide-out" style="transform: translateX(-100%);">

    <div class="text-center mb-xxl-4">
        <a href="#" class="logo-wrapper waves-effect">
            <img src="<?php echo e($logoUrl); ?>" onerror="this.src='<?php echo e(JVB_LOGO_URL); ?>'" class="img-fluid" alt="">
        </a>

        <p><strong class="text-uppercase text-primary">
                <?php echo e($name); ?>

            </strong></p>
    </div>
    <div class="list-group list-group-flush" style="margin: 0 -15px">
        <a href="/"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['default']) ? 'active': ''); ?>">
            <i class="fas fa-home mr-3"></i> <?php echo e(__l('Dashboard')); ?>

        </a>
        <a href="<?php echo e(route('regulation')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['regulation', 'regulation_detail']) ? 'active': ''); ?>">
            <i class="fas fa-anchor mr-3"></i> <?php echo e(__l('regulation')); ?></a>
        <a href="<?php echo e(route('contact')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['contact']) ? 'active': ''); ?>">
            <i class="fas fa-address-book mr-3"></i> <?php echo e(__l('contact')); ?></a>

        <a href="<?php echo e(route('project')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['project', 'project_detail']) ? 'active': ''); ?>">
            <i class="fas fa-industry mr-3"></i> <?php echo e(__l('Project')); ?></a>
        <a href="<?php echo e(route('meetings')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['meetings']) ? 'active': ''); ?>">
            <i class="fas fa-building mr-3"></i> <?php echo e(__l('Lịch họp')); ?>

        </a>
        <a href="<?php echo e(route('list_share_document')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['list_share_document']) ? 'active': ''); ?>">
            <i class="fas fa-file mr-3"></i> Chia sẻ tài liệu</a>
        <a href="<?php echo e(route('share_experience')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['share_experience']) ? 'active': ''); ?>">
            <i class="fas fa-book mr-3"></i> Kinh nghiệm làm việc</a>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('team-leader')): ?>
            <a href="<?php echo e(route('list_suggestions')); ?>"
               class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['list_suggestions']) ? 'active': ''); ?>">
                <i class="fas fa-lightbulb mr-3"></i> Đề xuất & góp ý</a>
        <?php endif; ?>
        <a href="<?php echo e(route('event')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['event', 'event_detail']) ? 'active': ''); ?>">
            <i class="fas fa-calendar mr-3"></i> <?php echo e(__l('Event')); ?>

        </a>
        <a href="<?php echo e(route('device_index')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['device_index']) ? 'active': ''); ?>">
            <i class="fas fa-desktop mr-3"></i>Đề xuất thiết bị
        </a>
        <a href="<?php echo e(route('labor_calendar_index')); ?>"
           class="list-group-item list-group-item-action waves-effect <?php echo e(\App\Utils::checkRoute(['labor_calendar_index']) ? 'active': ''); ?>">
            <i class="fa fas fa-trash mr-3"></i> Lịch trực nhật
        </a>
    </div>
</div>
<!-- Sidebar -->
