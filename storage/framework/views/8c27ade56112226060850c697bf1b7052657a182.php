<div style="display: inline-block;margin-right: 5px">
    <select class="mr-1  form-control" name="month" style="height: 32px">
        <?php $__currentLoopData = MONTH_OF_YEAR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number => $month): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($number); ?>" <?php echo e($number == $searchMonth ? 'selected' : ''); ?>><?php echo e($month); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>
<div style="display: inline-block;margin-right: 5px">
    <select class="mr-1  form-control" name="year" style="height: 32px">
        <?php $__currentLoopData = LABOR_YEAR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($year); ?>" <?php echo e($year == $searchYear ? 'selected' : ''); ?>>Năm <?php echo e($year); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>
<div class="input-group-btn" style="display: inline-block">
    <button type="submit" class="btn btn-sm btn-primary" style="height: 30px; margin-bottom: 7px;margin-right: 5px;"><i class="fa fa-search"></i> Tìm kiếm</button>
</div>
