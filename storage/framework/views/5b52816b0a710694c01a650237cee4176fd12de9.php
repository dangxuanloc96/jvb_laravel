<div class="text-right">
    <a href="<?php echo e(route('admin::posts.broadcast')); ?>" class="btn btn-success btn-table">Gửi thông báo nhanh</a>
</div>
<div class="table-responsive list-records">
    <table class="table table-hover table-bordered">
        <colgroup>
            <col style="width: 30px">
            <col style="width: 50px">
            <col style="width: 150px">
            <col style="width: 200px">
            <col style="">
            <col style="width: 120px">
            <col style="width: 100px">
            <col style="width: 100px">
            <col style="width: 90px">
            <col style="width: 100px">
        </colgroup>
        <thead>
        <th style="width: 10px;">
            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o "></i>
            </button>
        </th>
        <th>Ảnh thông báo</th>
        <th>Tên thông báo
            <?php echo __admin_sortable('name'); ?>

        </th>
        <th>Tóm tắt</th>
        <th>Người đăng</th>
        <th>Ngày tạo
            <?php echo __admin_sortable('created_at'); ?>

        </th>
        <th>Thông báo
            <?php echo __admin_sortable('notify_date'); ?>

        </th>
        <th>Kích hoạt</th>
        <th>Actions</th>
        </thead>
        <tbody>
        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
            $editLink = route($resourceRoutesAlias . '.edit', $record->id);

            $deleteLink = route($resourceRoutesAlias . '.destroy', $record->id);
            $formId = 'formDeleteModel_' . $record->id;
            ?>
            <tr>
                <td><input type="checkbox" name="ids[]" value="<?php echo e($record->id); ?>" class="square-blue chkDelete"></td>
                <td class="text-center">
                    <img src="<?php echo e(lfm_thumbnail($record->image_url)); ?>" width="100">
                </td>
                <td class="table-text">
                    <a href="<?php echo e($editLink); ?>"><?php echo e($record->name); ?></a>
                </td>
                <td><?php echo e($record->introduction); ?></td>
                <td><?php echo e($record->author_name); ?></td>
                <td class="text-right"><?php echo e($record->created_at->format(DATE_FORMAT)); ?></td>
                <td><span class="label label-success"><?php echo e($record->notify_date); ?></span></td>
                <?php if($record->status == 1): ?>
                    <td><span class="label label-info">Yes</span></td>
                <?php else: ?>
                    <td><span class="label label-warning">No</span></td>
            <?php endif; ?>

            <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <div class="btn-group">
                        <a href="<?php echo e($editLink); ?>" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete"
                           data-form-id="<?php echo e($formId); ?>"><i class="fa fa-trash-o"></i></a>
                    </div>

                    <!-- Delete Record Form -->
                    <form id="<?php echo e($formId); ?>" action="<?php echo e($deleteLink); ?>" method="POST"
                          style="display: none;" class="hidden form-inline">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('DELETE')); ?>

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>

            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
