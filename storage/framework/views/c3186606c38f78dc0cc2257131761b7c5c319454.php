<div class="box-footer clearfix">
    <div class="row">
        <div class="col-md-5">
            <div class="row pagination-row">
                <div class="col-md-3">
                    <select id="pageSize" name="per_page" class="pageSize form-control">
                        <?php $__currentLoopData = PAGE_LIST; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php if($item == $perPage): ?> selected
                                    <?php endif; ?> data-href="<?php echo e(str_replace('page', 'per_page', $records->url($item))); ?>"><?php echo e($item); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="col-md-9">
                    bản ghi trên một trang.
                </div>
            </div>

        </div>
        <div class="col-md-7">
            <!-- Pagination -->
            <div class="pull-right">
                <div class="no-margin text-center">
                    <?php echo $records->render(); ?>

                </div>
            </div>
            <!-- / End Pagination -->
        </div>
    </div>


</div>
<!-- /.box-footer -->
