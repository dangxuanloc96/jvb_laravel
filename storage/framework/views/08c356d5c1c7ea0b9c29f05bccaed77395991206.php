<?php
$postCount = \App\Models\Post::count();
$projectCount = \App\Models\Project::count();
$regulationCount = \App\Models\Regulation::count();
$teams = \App\Models\Team::select('id', 'name', 'color')->withCount('members')->get();
?>


<?php $__env->startSection('page-title', 'Admin page'); ?>
<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render('admin::admins'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo e(number_collapse($userCount)); ?></h3>

                    <p>Nhân viên</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="/admin/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo e(number_collapse($postCount)); ?></h3>

                    <p>Thông báo</p>
                </div>
                <div class="icon">
                    <i class="fa fa-bell"></i>
                </div>
                <a href="/admin/topics" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo e(number_collapse($projectCount)); ?></h3>

                    <p>Dự án</p>
                </div>
                <div class="icon">
                    <i class="fa fa-anchor"></i>
                </div>
                <a href="/admin/questions" class="small-box-footer">More info <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo e(number_collapse($regulationCount)); ?></h3>

                    <p>Nội quy/quy định</p>
                </div>
                <div class="icon">
                    <i class="fa fa-quote-right"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin tiền phạt</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <div class="btn-group">
                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo e(route('admin::index', ['type' => 1])); ?>">Tuần này</a></li>
                                <li><a href="<?php echo e(route('admin::index', ['type' => 2])); ?>">Tháng này</a></li>
                                <li><a href="<?php echo e(route('admin::index', ['type' => 3])); ?>">Năm nay</a></li>
                                <li><a href="<?php echo e(route('admin::index', ['type' => 4])); ?>">Năm trước</a></li>
                            </ul>
                        </div>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <p class="text-center">
                        <?php
                            switch (request('type')){
                                case 1:
                                    $punishText = 'Tiền phạt tuần này';
                                    break;
                                case 2:
                                    $punishText = 'Tiền phạt tháng này';
                                    break;
                                case 3:
                                    $punishText = 'Tiền phạt năm nay';
                                    break;
                                case 4:
                                    $punishText = 'Tiền phạt năm trước';
                                    break;
                                default:
                                    $punishText = 'Tiền phạt 6 tháng gần nhất';
                                    break;
                            }
                        ?>
                        <strong><?php echo e($punishText); ?></strong>
                    </p>

                    <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <canvas id="punishChart" style="height: 180px; width: 703px;" width="703"
                                height="180"></canvas>
                    </div>
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">

                                <h5 class="description-header"><?php echo e(number_format($totalLate)); ?></h5>
                                <span class="description-text">Phạt đi muộn</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <h5 class="description-header"><?php echo e(number_format($totalReport)); ?></h5>
                                <span class="description-text">Gửi báo cáo tuần</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <h5 class="description-header"><?php echo e(number_format($punishes->sum('total'))); ?></h5>
                                <span class="description-text">Tổng tiền phạt</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block">
                                <h5 class="description-header"><?php echo e(number_format($totalSubmit)); ?></h5>
                                <span class="description-text">Đã thu</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-4">
            <!-- USERS LIST -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Nhân viên thử việc</h3>

                    <div class="box-tools pull-right">
                        <span class="label label-danger"><?php echo e($probationStaffs->count()); ?> nhân viên</span>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <?php if($probationStaffs->isNotEmpty()): ?>
                        <ul class="users-list clearfix">
                            <?php $__currentLoopData = $probationStaffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $probationStaff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <img src="<?php echo e($probationStaff->avatar); ?>" onerror="this.src='<?php echo e(URL_IMAGE_NO_AVATAR); ?>'"
                                         alt="<?php echo e($probationStaff->name); ?>">
                                    <a class="users-list-name" href="#"><?php echo e($probationStaff->name); ?></a>
                                    <span class="users-list-date"><?php echo e($probationStaff->probation_at); ?></span>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php else: ?>
                        <h4 class="col-md-12">Không có nhân viên thử việc</h4>
                <?php endif; ?>
                <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="/admin/users" class="uppercase">Xem tất cả nhân viên</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <div class="col-sm-6 col-lg-4">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Teams</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="chart-responsive">
                                <canvas id="pieChart" height="155" width="205"
                                        style="width: 205px; height: 155px;"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <ul class="chart-legend clearfix">
                                <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <i class="fa fa-circle-o" style="color: <?php echo e($team->color); ?>"></i> <?php echo e($team->name); ?>

                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php $__currentLoopData = CONTRACT_TYPES_NAME; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type => $typeName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a href="#"><?php echo e($typeName); ?>

                                    <span class="pull-right"> <?php echo e(\App\Models\User::where('contract_type', $type)->where('status',ACTIVE_STATUS)->count()); ?></span>
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <!-- /.footer -->
            </div>
        </div>
        <div class="col-sm-6 col-lg-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sự kiện</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?php echo e(lfm_thumbnail($event->image_url)); ?>"
                                         alt="<?php echo e($event->name); ?>">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)"
                                       class="product-title"><?php echo e($event->name); ?>

                                        <span class="label label-warning pull-right"><?php echo e($event->event_date); ?></span></a>
                                    <span class="product-description">
                          <?php echo e($event->introduction); ?>

                        </span>
                                </div>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="<?php echo e(route('admin::events.index')); ?>" class="uppercase">Xem tất cả sự kiện</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-scripts'); ?>
    <script>
        window.punishChartData = {
            labels: [
                <?php $__currentLoopData = $punishes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $punish): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    'Tháng <?php echo e($punish->month); ?>',
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            ],
            datasets: [
                // {
                //     label: 'Electronics',
                //     fillColor: 'rgb(210, 214, 222)',
                //     strokeColor: 'rgb(210, 214, 222)',
                //     pointColor: 'rgb(210, 214, 222)',
                //     pointStrokeColor: '#c1c7d1',
                //     pointHighlightFill: '#fff',
                //     pointHighlightStroke: 'rgb(220,220,220)',
                //     data: [65, 59, 80, 81, 56, 55, 40]
                // },
                {
                    label: 'Tiền phạt',
                    fillColor: 'rgba(60,141,188,0.9)',
                    strokeColor: 'rgba(60,141,188,0.8)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: [
                        <?php $__currentLoopData = $punishes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $punish): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            '<?php echo e($punish->total / 1000); ?>',
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ]
                }
            ]
        };

        window.PieData = [
                <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
                value: '<?php echo e($team->members_count); ?>',
                color: '<?php echo e($team->color); ?>',
                highlight: '<?php echo e($team->color); ?>',
                label: '<?php echo e($team->name); ?>'
            },
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        ];

    </script>
    <script src="<?php echo e(asset_ver('js/libs/chart.js')); ?>"></script>
    <script src="<?php echo e(asset_ver('js/admin/dashboard.js')); ?>"></script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>