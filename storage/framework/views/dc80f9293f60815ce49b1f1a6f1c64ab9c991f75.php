<?php $__env->startSection('page-title', __l('Home')); ?>

<?php $__env->startSection('content'); ?>
    <div id="home">
        <section class="">
            <div class="row mt-md-5">
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class="post-item grey lighten-4">
                            <div class="row mb-3">
                                <div class="col-sm-5 text-center view overlay d-flex align-items-center">
                                    <a href="<?php echo e(route('post_detail', ['id' => $post->id])); ?>" class="w-100">
                                        <img class=""
                                             src="<?php echo e($post->image_url); ?>"
                                             alt="<?php echo e($post->name); ?>" width="100%">
                                        <div class="mask rgba-white-slight"></div>

                                    </a>
                                </div>
                                <div class="col-sm-7">
                                    <div class="media-body p-1"
                                         onclick="location.href='<?php echo e(route('post_detail', ['id' => $post->id])); ?>'">
                                        <h4 class="mt-3 mb-1 font-weight-bold elipsis-line line-2 fix-2 f-22"><?php echo e($post->name); ?></h4>
                                        <p class="elipsis-line line-3 fix-3 m-0"><?php echo e(str_limit(strip_tags(nl2br($post->introduction) ), 60)); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="text-right"><a href="<?php echo e(route('post')); ?>">Xem thêm thông báo >></a></div>
            <!-- Grid row -->
            <div class="row my-3">
                <!--Grid column-->
                <div class="col-xl-8 mb-4">
                    <?php if($events): ?>
                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="row mb-4">
                                <div class="col-lg-5">
                                    <div class="view overlay rounded mb-lg-0 mb-4">
                                        <img class="img-fluid img-event-home" src="<?php echo e($event->image_url); ?>"
                                             alt="<?php echo e($event->name); ?>">
                                        <a href="<?php echo e(route('event_detail', ['id' => $event->id])); ?>">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-7 home-event-content-right">
                                    <h3 class="font-weight-bold mb-1 home-title-event mb-1-18inch">
                                        <?php if($event->event_date > date('Y-m-d')): ?>
                                            <strong>[Sắp diễn ra] - <?php echo e($event->name); ?></strong>
                                        <?php else: ?>
                                            <strong><?php echo e($event->name); ?></strong>
                                        <?php endif; ?>
                                    </h3>
                                    <p class="mb-0">Thời gian tổ chức: <span
                                                class="text-danger"><?php echo e($event->event_date); ?></span></p>
                                    <p class="mb-0">Địa điểm tổ chức: <span
                                                class="text-danger"><?php if($event->place): ?><?php echo e(str_limit(strip_tags(nl2br($event->place) ), 30)); ?><?php else: ?>
                                                Bí mật <?php endif; ?></span></p>
                                    <hr class="my-1 my-3-18inch">
                                    <p class="d-none-15inch"><?php echo e(str_limit(strip_tags(nl2br($event->introduction) ), 150)); ?></p>
                                    <p class="d-none-18inch mb-15ich-0"><?php echo e(str_limit(strip_tags(nl2br($event->introduction) ), 100)); ?></p>
                                    <a class="btn btn-warning btn-md ml-0"
                                       href="<?php echo e(route('event_detail', ['id' => $event->id])); ?>">Xem chi tiết</a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <a class="btn btn-primary waves-effect waves-light mb-4" style="margin-right: 0px"
                           href="<?php echo e(route('event')); ?>" role="button"> Xem tất cả
                            sự kiện</a>
                <?php endif; ?>

                <?php echo $__env->make('elements.feedback', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!--Card-->
                    <!--/.Card-->
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 mb-4">
                    <?php if(!empty($laborUserName)): ?>
                        <?php echo $__env->make('elements.day_labor_calendar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                    <?php echo $__env->make('elements.punish', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <ul class="list-group">
                        <li class="list-group-item active text-center">
                            <strong class="text-uppercase">Dự án mới</strong>
                        </li>
                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="list-group-item grey lighten-4">
                                <strong><a class="text-black"
                                           href="<?php echo e(route('project_detail', ['id' => $project->id])); ?>"><?php echo e($project->name); ?></a></strong>
                                <div>
                                    <?php echo nl2br($project->technical); ?>

                                </div>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>

                </div>

            </div>
            <!--Grid row-->
        </section>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.end_user', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>