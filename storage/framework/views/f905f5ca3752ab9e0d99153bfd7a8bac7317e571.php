<?php if(env('ENABLE_PUSH_NOTIFY', false)): ?>
    <script>
        window.messagingSenderId = "<?php echo e(env('FIREBASE_SENDER_ID')); ?>";
    </script>
    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/6.1.0/firebase.js"></script>

    <script>
        // TODO: Replace the following with your app's Firebase project configuration
        var firebaseConfig = {
            apiKey: "<?php echo e(env('FIREBASE_APP_KEY')); ?>",
            authDomain: "<?php echo e(env('FIREBASE_PROJECT_ID')); ?>.firebaseapp.com",
            databaseURL: "https://<?php echo e(env('FIREBASE_PROJECT_ID')); ?>.firebaseio.com",
            projectId: "<?php echo e(env('FIREBASE_PROJECT_ID')); ?>",
            storageBucket: "<?php echo e(env('FIREBASE_PROJECT_ID')); ?>.appspot.com",
            messagingSenderId: "<?php echo e(env('FIREBASE_SENDER_ID')); ?>",
            appID: "<?php echo e(env('FIREBASE_APP_ID')); ?>",
        };
        window.firebaseToken = null;
        window.firebaseTokenId = null;
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        const messaging = firebase.messaging();
        messaging
            .requestPermission()
            .then(function () {

                // get the token in the form of promise
                return messaging.getToken()
            })
            .then(function (token) {
                window.firebaseToken = token;
                $.ajax({
                    url: '<?php echo e(route('notification_save_token')); ?>',
                    method: 'POST',
                    dataType: 'JSON',
                    data: {notify_token: token},
                    success: function (data) {
                        window.firebaseTokenId = data.id;
                    }
                });

                //save tokenNgười tạo
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.", err);
            });

        messaging.onMessage(function (payload) {
            $("#favicon").attr("href", "/img/favicons/favicon-unread.ico");
            blinkNotificationCount();
            var notification = new Notification(payload.notification.title, {
                body: payload.notification.body,
                icon: payload.data.icon || '<?php echo e(JVB_LOGO_URL); ?>'
            });

            notification.onclick = function () {
                window.open(payload.data.url || '<?php echo e(env('APP_URL')); ?>');
            }
        });
    </script>
<?php endif; ?>
