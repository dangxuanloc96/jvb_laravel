<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>

    <meta property="og:image" content="<?php echo e(JVB_LOGO_URL); ?>">
    <meta itemprop="image" content="<?php echo e(JVB_LOGO_URL); ?>">
    <?php echo $__env->make('layouts.partials.frontend.meta', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <title><?php echo e(config('app.name', 'BBS')); ?> <?php if (! empty(trim($__env->yieldContent('page-title')))): ?> | <?php echo $__env->yieldContent('page-title'); ?> <?php endif; ?></title>

    <!-- Styles -->
    <link href="<?php echo e(asset_ver('css/complied.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="<?php echo e(asset_ver('mdb/css/addons/datatables.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset_ver('css/notification.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset_ver('css/style.css')); ?>" rel="stylesheet">

    <script type="text/javascript" src="<?php echo e(asset_ver('mdb/js/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset_ver('mdb/js/popper.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset_ver('mdb/js/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset_ver('mdb/js/addons/datatables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset_ver('js/sweetalert.min.js')); ?>"></script>
    <script type="text/javascript" src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <script>
      window.userId = '<?php echo e(\Illuminate\Support\Facades\Auth::id()); ?>';
      window.system_image = '<?php echo e(JVB_LOGO_URL); ?>';
      <?php if(config('app.env') != 'production'): ?>
        Pusher.logToConsole = true;
      <?php endif; ?>
        window.pusher = new Pusher("21bd3134dbf06bd45d45", {
        cluster: 'ap1',
        forceTLS: true,
        authEndpoint: '/broadcasting/auth',
        auth: {
          headers: {
            'X-CSRF-Token': "<?php echo e(csrf_token()); ?>"
          }
        }
      });
    </script>
    <?php echo $__env->yieldPushContent('extend-css'); ?>
</head>
<body>
<?php echo $__env->make('layouts.partials.frontend.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<main id="app" class="p-t-2-2">
    <div class="container-fluid mt-3 m-t-4em">
        <div id="main">
            <?php if(View::hasSection('breadcrumbs')): ?>
                <?php echo $__env->yieldContent('breadcrumbs'); ?>
            <?php endif; ?>
            <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
</main>
<!-- editor -->
<script src="<?php echo e(asset_ver('js/tinymce/tinymce.min.js')); ?>"></script>
<script type="text/javascript">
  (function ($) {
    if (document.head.querySelector('meta[name="csrf-token"]')) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    } else {
      console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }
  })(jQuery);
</script>
<?php echo $__env->yieldPushContent('footer-scripts'); ?>

<!-- Scripts -->
<script type="text/javascript" src="<?php echo e(asset_ver('js/mdb.min.js?v=1')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('js/moment-with-locales.min.js')); ?>"></script>

<?php echo $__env->make('layouts.push-notification', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript" src="<?php echo e(asset_ver('js/main.js')); ?>?v=2020"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('js/notify.js')); ?>"></script>
<?php echo $__env->yieldPushContent('extend-js'); ?>
</body>
</html>
