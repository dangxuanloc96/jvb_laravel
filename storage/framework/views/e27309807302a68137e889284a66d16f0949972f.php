<?php
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Thêm " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index');
$_createLink = route($resourceRoutesAlias . '.create');
$_storeLink = route($resourceRoutesAlias . '.store');
?>


<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render($resourceRoutesAlias.'.create'); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('page-title', $_pageTitle); ?>


<?php $__env->startSection('page-subtitle', $_pageSubtitle); ?>


<?php $__env->startSection('head-extras'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" id="form-main" role="form" method="POST" action="<?php echo e($_storeLink); ?>"
                      enctype="multipart/form-data" <?php echo $_formFiles === true ? 'enctype="multipart/form-data"' : ''; ?>>
                    <?php echo e(csrf_field()); ?>


                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới</h3>

                        <div class="box-tools">
                            <div style="display: inline-block;margin-right: 5px">
                                <select class="mr-1  form-control" name="month" style="height: 32px;padding-top: 2px;" onchange="getLaborUser(this)">
                                    <?php $__currentLoopData = MONTH_OF_YEAR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number => $month): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($number); ?>" <?php echo e($currentMonth == $number ? 'selected' : ''); ?>><?php echo e($month); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div style="display: inline-block">
                                <select class="mr-1  form-control" name="year" style="height: 32px;padding-top: 2px" onchange="getLaborUser(this)">
                                    <?php $__currentLoopData = LABOR_YEAR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($year); ?>" <?php echo e($currentYear == $year ? 'selected' : ''); ?>>Năm <?php echo e($year); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <a href="<?php echo e($_listLink); ?>" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="<?php echo e($_createLink); ?>" class="btn btn-sm btn-success margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            <a class="btn btn-sm btn-info margin-r-5 margin-l-5" onclick="checkRequest()">
                                <i class="fa fa-save"></i> <span>Lưu</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <?php echo $__env->make($resourceAlias.'.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                    <!-- /.box-body -->
                    <?php if($resourceRoutesAlias != 'admin::day_offs'): ?>

                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with">
                                <div align="center">
                                    <a class="btn btn-info mr-2" onclick="checkRequest()">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </a>
                                    <a href="<?php echo e($_listLink); ?>" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                        <!-- /.box-footer -->
                    <?php endif; ?>
                </form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-extras'); ?>
    <script>
        function countUserChecked()
        {
            let countChecked = 0;
            $.each($('input[type=checkbox]'),function (key, value) {
                if($(this).is(':checked'))
                {
                    countChecked++;
                }
            });
            return countChecked;
        }

        function getLaborUser(e){
            let month = $('select[name=month]').val();
            let year = $('select[name=year]').val();
            $('input[name=month]').val(month);
            $('input[name=year]').val(year);

            $.ajax({
                url: "<?php echo e(route('admin::count.user.chose')); ?>",
                type: 'post',
                data: {
                    month: month,
                    year: year
                },
                success: function (response) {
                    let countChecked = countUserChecked();
                    let chosePerTotal = countChecked+'/'+response;
                    $('.rate').html(chosePerTotal);
                    $('.rate').attr('total-user',response);

                }

            });
        }

        function checkRequest(){
            let content = tinyMCE.activeEditor.getContent();
            if(!content)
            {
                alert('Nội dung công việc không được để trống')
                return false;
            }

            let countChecked = 0;
            $.each($('input[type=checkbox]'),function (key, value) {
                if($(this).is(':checked'))
                {
                    countChecked++;
                }
            });
            let total = $('.rate').attr('total-user');
            if(countChecked < total)
            {
                alert('Số người trực nhật chưa đủ');
                return false;
            }
            $('#form-main').submit();
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>