<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <link rel="icon" type="image/png" href="/favicon.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('page-title'); ?> <?php if (! empty(trim($__env->yieldContent('page-subtitle')))): ?> | <?php echo $__env->yieldContent('page-subtitle'); ?> <?php endif; ?></title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo e(asset_ver('bootstrap/css/bootstrap.min.css')); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo e(asset_ver('font-awesome/css/font-awesome.min.css')); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo e(asset_ver('css/ionicons.min.css')); ?>">

    <!-- Plugins -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?php echo e(asset_ver('adminlte/plugins/iCheck/all.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Select2 -->
    <link href="<?php echo e(asset_ver('adminlte/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- datetimepicker -->
    <link href="<?php echo e(asset_ver('bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>"
          rel="stylesheet" type="text/css">
    <!-- datetimepicker -->
    <link href="<?php echo e(asset_ver('bootstrap-datepicker/css/bootstrap-datePicker.css')); ?>"
          rel="stylesheet" type="text/css">
    <!-- timepicker -->
    <link href="<?php echo e(asset_ver('bootstrap-datetimepicker/css/bootstrap-timepicker.min.css')); ?>"
          rel="stylesheet" type="text/css">
    <!-- END - Plugins -->
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo e(asset_ver('bootstrap-daterangepicker/daterangepicker.css')); ?>" rel="stylesheet" type="text/css">
    <!-- AdminLTE App -->
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset_ver('adminlte/css/AdminLTE.min.css')); ?>">
    <!-- AdminLTE Skin. -->

    <link rel="stylesheet" href="<?php echo e(asset_ver('adminlte/css/skins/' . config('adminlte.theme') . '.min.css')); ?>">

    <!-- Custom CSS -->
    <link href="<?php echo e(asset_ver('css/backend.css?version=' . config('adminlte.version'))); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- jQuery 3 -->
    <script src="<?php echo e(asset_ver('js/jquery.min.js')); ?>"></script>
    <!-- Moment Js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <?php echo $__env->yieldContent('head-extras'); ?>
</head>

<body class="hold-transition <?php echo e(config('adminlte.theme')); ?> sidebar-mini">
<?php if(auth()->guard()->check()): ?>
    <script type="text/javascript">
        /* Recover sidebar state */
        (function () {
            if (Boolean(localStorage.getItem('sidebar-toggle-collapsed'))) {
                var body = document.getElementsByTagName('body')[0];
                body.className = body.className + ' sidebar-collapse';
            }
        })();
    </script>
<?php endif; ?>

<!-- Site wrapper -->
<div class="wrapper">

<?php echo $__env->make('layouts.partials.backend.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('layouts.partials.backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.partials.backend.modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $__env->yieldContent('page-title'); ?>
                <small><?php echo $__env->yieldContent('page-subtitle'); ?></small>
            </h1>
            <?php echo $__env->yieldContent('breadcrumbs'); ?>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->yieldContent('content'); ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> <?php echo e(config('adminlte.version')); ?>

        </div>
        <strong>Copyright &copy; <?php echo e(date('Y')); ?>. <?php echo config('adminlte.credits'); ?></strong>. All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->
<input type="file" id="file_editor_upload" class="hidden">

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset_ver('bootstrap/js/bootstrap.min.js')); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo e(asset_ver('adminlte/plugins/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset_ver('adminlte/plugins/fastclick/fastclick.js')); ?>"></script>

<!-- Plugins -->
<!-- iCheck for checkboxes and radio inputs -->
<script src="<?php echo e(asset_ver('adminlte/plugins/iCheck/icheck.min.js')); ?>" type="text/javascript"></script>
<!-- Select2 -->
<script src="<?php echo e(asset_ver('adminlte/plugins/select2/js/select2.min.js')); ?>" type="text/javascript"></script>

<!-- DatetimePicker Js-->
<script src="<?php echo e(asset_ver('bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset_ver('bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>"></script>
<!-- END - Plugins -->
<!-- Timepicker -->
<script src="<?php echo e(asset_ver('bootstrap-datetimepicker/js/bootstrap-timepicker.min.js')); ?>"></script>
<!-- AdminLTE App -->
<!-- Chart JS -->
<script src="<?php echo e(asset_ver('chart/js/Chart.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset_ver('chart/js/utils.js')); ?>"></script>
<script src="<?php echo e(asset_ver('chart/js/chartjs-plugin-labels.js')); ?>" type="text/javascript"></script>
<!-- End Chart JS -->

<!-- bootstrap-daterangepicker -->
<script src="<?php echo e(asset_ver('bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- AdminLTE App -->

<script src="<?php echo e(asset_ver('adminlte/js/adminlte.min.js')); ?>"></script>
<!-- Custom Js -->
<script src="<?php echo e(asset_ver('js/backend.js?version=' . config('adminlte.version'))); ?>"></script>
<script src="<?php echo e(asset_ver('js/tinymce/tinymce.min.js')); ?>"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script type="text/javascript">
    (function ($) {
        if (document.head.querySelector('meta[name="csrf-token"]')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        } else {
            console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
        }
    })(jQuery);
</script>
<link href="<?php echo e(asset_ver('adminlte/css/multiselect.css')); ?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js"></script>
<script type="text/javascript" src="<?php echo e(asset_ver('adminlte/js/multiselect.min.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // make code pretty
        window.prettyPrint && prettyPrint();
        $('#undo_redo').multiselect();
    });
</script>
<script src="<?php echo e(asset_ver('js/admin.js')); ?>"></script>
<?php echo $__env->yieldContent('footer-extras'); ?>

<?php echo $__env->yieldPushContent('footer-scripts'); ?>

</body>
</html>
