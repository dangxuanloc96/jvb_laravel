<?php $__env->startSection('breadcrumbs'); ?>
    <?php echo Breadcrumbs::render('labor_calendar'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
            $date = $timeSearch->toDateString();
            $startDay_1 = \Carbon\Carbon::parse($date)->firstOfMonth();
            $startDay_2 = \Carbon\Carbon::parse($date)->firstOfMonth();
            $endDay_1 = \Carbon\Carbon::parse($date)->endOfMonth();
            $endDay_2 = \Carbon\Carbon::parse($date)->endOfMonth();
    ?>

    <div class="card">
        <form id="labor-form" action="<?php echo e(route('labor_calendar_index')); ?>" method="GET">
            <h5 class="card-header h5" style="position: relative" >Lịch trực nhật tháng
                <?php echo e($timeSearch->format(MONTH_YEAR_FORMAT)); ?>

                <div style="display: inline-block; position: absolute; right: 10px;top: 10px">
                    <select style="display: block !important;" onchange="findToMonth()" name="month">
                        <?php $__currentLoopData = MONTH_OF_YEAR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $monthName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e($month == $key ? 'selected' : ''); ?>><?php echo e($monthName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </h5>
        </form>
        <?php if(!empty($listLaborCalendars)): ?>
        <div class="card-body" style="margin: auto">

            <table class="table table-bordered text-center" style="border-collapse: collapse; max-width: 900px; height: 363px; width: 900px;" border="0"  cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="secondary-color text-white bolder" style="height: 15.0pt;">
                    <td style="height: 15px; width: 101px;" height="20"><strong><span class="font5">Chủ nhật</span></strong></td>
                    <td style="width: 102px; height: 15px;"><strong><span class="font5">Thứ 2</span></strong></td>
                    <td style="width: 109px; height: 15px;"><strong><span class="font5">Thứ 3</span></strong></td>
                    <td class="xl65" style="width: 117px; height: 15px;"><strong><span class="font5">Thứ 4</span></strong></td>
                    <td class="xl65" style="border-left: none; width: 98px; height: 15px;"><strong><span class="font5">Thứ 5</span></strong></td>
                    <td class="xl65" style="border-left: none; width: 107px; height: 15px;"><strong><span class="font5">Thứ 6</span></strong></td>
                    <td class="xl65" style="border-left: none; width: 79px; height: 15px;"><strong><span class="font5">Thứ 7</span></strong></td>
                </tr>
                <?php while(true): ?>
                <tr class="indigo lighten-2 text-white" style="height: 15.0pt;">
                    <?php for($j = 0; $j <= 6; $j++): ?>
                            <?php if($startDay_1->dayOfWeek == $j && $startDay_1 <= $endDay_1): ?>
                                <td class="xl65" style="width: 117px; height: 15px;" ><strong><?php echo e($startDay_1->format(DATE_MONTH_REPORT)); ?></strong></td>
                                <?php $startDay_1->addDays(1) ?>
                            <?php else: ?>
                                <td style="height: 15px; width: 101px;" height="20">&nbsp;</td>
                            <?php endif; ?>
                    <?php endfor; ?>
                </tr>
                <tr style="height: 12.75pt;">
                    <?php for($j = 0; $j <= 6; $j++): ?>
                        <?php if($startDay_2->dayOfWeek == $j && $startDay_2 <= $endDay_2): ?>
                            <?php if($startDay_2->dayOfWeek !== DAY_OF_WEEK['sunday'] && $startDay_2->dayOfWeek !== DAY_OF_WEEK['saturday']): ?>
                                <td class="xl66"  style="border-top: none; width: 117px; height: 12px;">
                                    <?php $__currentLoopData = $listLaborCalendars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $laborCalendar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(array_key_exists($startDay_2->format(DATE_MONTH_REPORT),$laborCalendar)): ?>
                                            <p check-self-color="<?php echo e($laborCalendar['checkSelf']); ?>"><?php echo $laborCalendar[$startDay_2->format(DATE_MONTH_REPORT)]; ?></p>
                                            <?php if(!empty($laborCalendar['note'])): ?>
                                            <div><i class="fas fa-exclamation-triangle"></i> <?php echo $laborCalendar['note']; ?></div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </td>
                                <?php else: ?>
                                <td style="height: 12px; width: 101px;vertical-align: middle" height="17"><i class="fas fa-calendar-times"></i></td>
                            <?php endif; ?>
                            <?php $startDay_2->addDays(1) ?>
                        <?php else: ?>
                            <td style="height: 12px; width: 101px;vertical-align: middle" height="17"><i class="fas fa-calendar-times"></i></td>
                        <?php endif; ?>
                    <?php endfor; ?>
                </tr>
                <?php if($startDay_1 > $endDay_1): ?>
                    <?php break; ?>
                <?php endif; ?>
                <?php endwhile; ?>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p>Danh sách công việc thực hiện:</p>
            <?php echo $workContent; ?>

        </div>
        <?php else: ?>
        <h5 style="padding: 20px" >Chưa có lịch trực nhật trong tháng này</h5>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-scripts'); ?>
    <script >
        function findToMonth()
        {
            $('#labor-form').submit();
        }

        $('p[check-self-color]').each(function (index, value) {
                if($(this).attr('check-self-color') == true)
                {
                    $(this).closest('td').css('background-color','yellow');
                }
        });
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.end_user', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>