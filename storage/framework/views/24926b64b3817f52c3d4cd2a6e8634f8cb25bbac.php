<?php $__env->startSection('js-extend'); ?>
    <script>
        $(function () {
            window.setTimeout(function () {
                $("#email").next().addClass('active')
                $("#password").next().addClass('active')
            }, 500);
        })
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="d-none d-xl-block">
            <br/>
            <br/>
            <br/>
        </div>
        <div class="row mt-5">

            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6">
                <?php if(Session::has('notification_change_pass')): ?>
                    <div class="d-flex justify-content-center">
                        <div class="alert alert-success">
                            <?php echo e(Session::get('notification_change_pass')); ?>

                        </div>
                    </div>
            <?php endif; ?>
            <!-- Material form login -->
                <div class="card">

                    <h5 class="card-header info-color white-text text-center py-4">
                        <strong>BBS System</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-4">
                        <!-- Form -->
                        <form class="text-center" style="color: #757575;" method="POST" action="<?php echo e(route('login')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <?php if($error = $errors->first('email')): ?>
                                <div class="alert alert-danger ">
                                    <?php echo e($error); ?>

                                </div>
                                <br/>
                        <?php endif; ?>

                        <!-- Email -->
                            <div class="md-form mt-2">
                                <input type="text" id="email" name="email" class="form-control" required
                                       value="<?php echo e(old('email')); ?>">
                                <label for="email">E-mail</label>
                            </div>

                            <!-- Password -->
                            <div class="md-form">
                                <input type="password" id="password" name="password" class="form-control" required>
                                <label for="password">Mật khẩu</label>
                            </div>

                            <div class="d-flex justify-content-around">
                                <div>
                                    <!-- Remember me -->
                                    <label class="pure-material-checkbox">
                                        <input type="checkbox" name="remember">
                                        <span>Nhớ đăng nhập</span>
                                    </label>
                                </div>
                                <div>
                                    <!-- Forgot password -->
                                    <a href="<?php echo e(url('/password/reset')); ?>">Quên mật khẩu?</a>
                                </div>
                            </div>

                            <!-- Sign in button -->
                            <button class="btn btn-info btn-rounded btn-block my-4 waves-effect z-depth-0"
                                    type="submit">Đăng nhập
                            </button>
                        </form>
                        <!-- Form -->

                    </div>

                </div>
                <!-- Material form login -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>