<?php

namespace App\Repositories;

use App\Models\Reminder;
use App\Models\ReminderMember;
use App\Repositories\Contracts\IReminderRepository;
use Illuminate\Http\Request;
use Auth;
use DB;

/**
 * RegulationRepository class
 * Author: jvb
 * Date: 2019/08/05
 */
class ReminderRepository extends AbstractRepository implements IReminderRepository
{
    /**
     * RegulationModel
     *
     * @var  string
     */
    protected $modelName = Reminder::class;

    public function updateOneBy($id, array $attributes)
    {
    	$result = $this->findOne($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }
        return false;   
    }

    /**
     * [getStatus description]
     * @return [type] [description]
     */
    public function getStatusUnsuccess()
    {   
        $userId = Auth::id();
        $reminders = DB::table('reminders')
        ->select(
           'reminders.id',
           'reminders.name',
           'reminders.content',
           'reminders.start_date',
           'reminders.start_loop',
           'reminders.end_loop',
           'reminders.option_loop',
           'reminders.deleted_at',
           'reminders.status',
           'reminders.route_detail_reminder',
           'reminders.meeting_deleted')
        ->join('reminder_members', 'reminders.id', '=', 'reminder_members.reminder_id')
        ->join('users', 'reminder_members.user_id', '=', 'users.id')
        ->where('reminders.status', UNACTIVE_STATUS)
        ->where('users.id', $userId)
        ->where('reminders.deleted_at',null)
        ->where('reminders.meeting_deleted', 0)
        ->paginate(9);    
        return $reminders;
    }

    /**
     * [getSuccessfull description]
     * @return [type] [description]
     */
    public function getStatusSuccess()
    {
        $userId = Auth::id();
        $reminders = DB::table('reminders')
        ->select(
            'reminders.id',
            'reminders.name',
            'reminders.content',
            'reminders.start_date',
            'reminders.start_loop',
            'reminders.end_loop',
            'reminders.option_loop',
            'reminders.deleted_at',
            'reminders.status',
            'reminders.meeting_deleted')
        ->join('reminder_members', 'reminders.id', '=', 'reminder_members.reminder_id')
        ->join('users', 'reminder_members.user_id', '=', 'users.id')
        ->where('reminders.status', ACTIVE_STATUS)
        ->where('users.id', $userId)
        ->where('reminders.deleted_at',null)
        ->where('reminders.meeting_deleted', 0)
        ->paginate(10);    
        return $reminders;
    }
}
