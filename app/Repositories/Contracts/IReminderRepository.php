<?php 
namespace App\Repositories\Contracts;

/**
* GroupRepository contract.
* Author: jvb
* Date: 2019/08/05 
*/
interface IReminderRepository extends IBaseRepository {
	
	/**
	 * [updateOne description]
	 * @param  [type] $id         [description]
	 * @param  array  $attributes [description]
	 * @return [type]             [description]
	 */
	public function updateOneBy($id, array $attributes);	

	public function getStatusUnsuccess();
	public function getStatusSuccess();
}
