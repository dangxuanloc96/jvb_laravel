<?php

namespace App\Repositories;

use App\Models\LaborCalendar;
use App\Models\User;
use App\Repositories\Contracts\IUserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * UserRepository class
 * Author: jvb
 * Date: 2018/07/16 10:34
 */
class UserRepository extends AbstractRepository implements IUserRepository
{
    /**
     * UserModel
     *
     * @var  string
     */
    protected $modelName = User::class;

    public function getMemberName($id)
    {
        return $this->getModel()->where('id', $id)->first()->name;
    }

    public function getLaborUserList()
    {
        $listUser = $this->getModel()->where('contract_type','!=',CONTRACT_TYPES['internship'])->get();
        $arrayInfo = [];
        foreach ($listUser as $user)
        {
            $infoUser = [];
            $infoUser ['user'] = $user;
            $laborDateInfo = [];
            for($i = 6 ; $i > 0 ; $i--)
            {
                $dateNow = Carbon::now();
                $beforeDate = $dateNow->modify("-$i months");
                $startDateOfMonth = $beforeDate->firstOfMonth()->toDateString();
                $endDateOfMonth = $beforeDate->endOfMonth()->toDateString();
                $checkLabor = DB::table('labor_calendars')
                    ->join('labor_calendar_details','labor_calendars.id','=','labor_calendar_details.labor_calendar_id')
                    ->whereBetween('labor_calendars.labor_date',[$startDateOfMonth,$endDateOfMonth])
                    ->where('labor_calendar_details.user_id',$user->id)
                    ->get();
                if(count($checkLabor))
                {
                    $laborDateInfo [] = true;
                }else{
                    $laborDateInfo [] = false;
                }
            }
            $infoUser ['info'] = $laborDateInfo;
            $arrayInfo [] = $infoUser;
        }
        return $arrayInfo;
    }
}
