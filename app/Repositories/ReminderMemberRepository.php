<?php

namespace App\Repositories;

use App\Models\ReminderMember;
use App\Repositories\Contracts\IReminderMemberRepository;

/**
 * RegulationRepository class
 * Author: jvb
 * Date: 2019/08/05
 */
class ReminderMemberRepository extends AbstractRepository implements IReminderMemberRepository
{
    /**
     * RegulationModel
     *
     * @var  string
     */
    protected $modelName = ReminderMember::class;
}
