<?php

namespace App\Console\Commands;

use App\Events\UserNotice;
use App\Helpers\NotificationHelper;
use App\Models\LaborCalendar;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DailyLaborScheduleNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:daily_labor_schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'notify labor schedule before a day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notificationDate = Carbon::now()->addDays(1);
        $notifications = [];
        $laborCalendar = LaborCalendar::where('labor_date',$notificationDate->format(DATE_FORMAT_SLASH))->first();
        $title = "Bạn có lịch trực nhật vào ngày mai(".$notificationDate->format(DATE_TIME_FORMAT_VI).")";
        $url = route('labor_calendar_index');
        if(!empty($laborCalendar))
        {
            foreach ($laborCalendar->laborCalendarDetail as $detail)
            {
                $user = User::find($detail->user_id);
                $notifications[] = NotificationHelper::generateNotify($detail->user_id, $title, 'test', 0, NOTIFICATION_TYPE['post'], $url);
                event(new UserNotice($user, $title, '', $url));
            }
        }
        if(!empty($notifications))
        {
            Notification::insertAll($notifications);
        }
    }
}
