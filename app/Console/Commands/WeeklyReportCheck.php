<?php

namespace App\Console\Commands;

use App\Helpers\DatabaseHelper;
use App\Models\CalendarOff;
use App\Models\Config;
use App\Models\Report;
use App\Models\User;
use App\Services\PunishesService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WeeklyReportCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weekly-report:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new booking follow bookings';

    /**
     * @var PunishesService
     */
    private $punishesService;

    /**
     * Create a new command instance.
     *
     * @param PunishesService $punishesService
     */
    public function __construct(PunishesService $punishesService)
    {
        parent::__construct();
        $this->punishesService = $punishesService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = Config::first();
        $now = Carbon::now();
        $day = (int)$now->format('N');
        $time = $now->format(TIME_FORMAT_ONLY);

        if ($config->enable_weekly_report_check
            && in_array($day, $config->weekly_report_check_day)
            && str_contains($config->weekly_report_check_time, $time)
        ) {
            $dayFormat = $now->format(DATE_FORMAT);
            $calendarOffs = CalendarOff::all();
            //check is holiday
            if ($calendarOffs->where('date_off_from', '<=', $dayFormat)->where('date_off_to', '>=', $dayFormat)->first()) {
                return;
            }
            $week = (int)$now->format('W');
            //check last week
            if ($day != 7) {
                $week--;
            }

            $monday = date(DATE_FORMAT, strtotime('monday last week'));
            $mondayD = Carbon::createFromFormat(DATE_FORMAT, $monday);
            //get total number workday in week
            $startEndDate = getStartAndEndDate($week, $mondayD->year, false);
            $startDate = $startEndDate['week_start'];
            $endDate = $startEndDate['week_end'];

            $dateLists = get_date_list($startDate, $endDate);
            $totalWorkDay = 0;
            foreach ($dateLists as $item) {
                if (!$calendarOffs->where('date_off_from', '<=', $item)->where('date_off_to', '>=', $item)->first())
                    $totalWorkDay++;
            }
            //workday in week are too short
            if ($totalWorkDay < 2) return;
            $userIds = Report::select('user_id')//->where('year', $mondayD->year)
            ->where('month', $mondayD->month)
                ->where('week_num', $week)
                ->whereNull('report_date')->pluck('user_id')->toArray();

            //sql select
            $userModel = User::select('id', 'name', 'jobtitle_id', 'is_remote')
                ->where('status', ACTIVE_STATUS)
                ->where('start_date', '<=', $monday)
                ->where('jobtitle_id', '<', TEAMLEADER_ROLE)
                ->whereNull('end_date')
                ->where(function ($q) {
                    $q->whereNull('is_remote')->orWhere('is_remote', 0);
                })
                ->whereNotIn('id', $userIds);
            $users = $userModel->get();

            Log::info(DatabaseHelper::getQuery($userModel));
            $this->punishesService->noWeeklyReport($dayFormat, $week, $users, $config->must_comfirm_weekly_report_check);
        }
    }
}
