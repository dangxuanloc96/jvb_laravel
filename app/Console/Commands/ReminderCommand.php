<?php

namespace App\Console\Commands;

use App\Helpers\NotificationHelper;
use App\Models\User;
use App\Models\UserFirebaseToken;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;

class ReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:reminder-notice {--any=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->notificationService = app()->make(NotificationService::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = Carbon::now('Asia/Ho_Chi_Minh')->toTimeString();
        $date = Carbon::now()->toDateString();
        $anyTime = $this->option('any');
        if ($anyTime)
            $intervalTime = 1;
        else
            $intervalTime = NOTIFICATION_REPEAT_MINUTE;
        //Select reminder loop
        $reminders = DB::table('reminder_members')
        ->join('reminders', 'reminder_members.reminder_id', 'reminders.id')
        ->join('users', 'reminder_members.user_id', 'users.id')
        ->select(
            'reminder_members.user_id',
            'reminders.id',
            'reminders.name',
            'reminders.content',
            'reminders.start_date',
            'reminders.start_loop',
            'reminders.end_loop',
            'reminders.last_notice',
            'reminders.option_loop',
            'reminders.route_detail_reminder',
            'reminders.status',
            'reminders.deleted_at',
            'reminders.meeting_deleted',
            'users.last_activity_at')
        ->where('reminders.deleted_at', '=', null)
        ->where('reminders.start_date', '<=', $date)
        ->where('reminders.start_loop', '<=', $time)
        ->where('reminders.end_loop', '>=', $time)
        ->where('reminders.status', UNACTIVE_STATUS)
        ->where('reminders.last_notice', '<=', $time)
        ->where('reminders.meeting_deleted', 0)
        ->where('users.status', ACTIVE_STATUS)
        ->where('users.last_activity_at', '<=', Carbon::now()->subMinute($intervalTime))
        ->get();     
        
        if($reminders) {
            $reminders_count = $reminders->count();
            $users = User::select('id', 'name', 'last_activity_at')
            ->has('firebase_tokens')
            ->with('firebase_tokens')
            ->get();

            foreach ($users as $user) {
                $devices = [];
                foreach ($user->firebase_tokens as $firebase_token) {
                    if ($anyTime || $firebase_token->push_at == null || $firebase_token->push_at <= Carbon::now()->subMinute($intervalTime)) {
                        $devices[] = $firebase_token->token;
                        $firebase_token->push_at = Carbon::now();
                        $firebase_token->save();
                    }
                }
                if (!empty($devices)) {
                    $content = 'Bạn có '.$reminders_count.'nhắc việc!';
                    NotificationHelper::sendPushNotification($devices, '[' . env('APP_NAME') . ']', $content);
                }
            }
        }
        $this->notificationService->sendReminderNotification($reminders, $time);
    }
}
