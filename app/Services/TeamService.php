<?php
/**
 * Created by PhpStorm.
 * User: muatu
 * Date: 1/23/2019
 * Time: 10:04 AM
 */

namespace App\Services;

use App\Repositories\Contracts\ITeamRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\ITeamService;
use Illuminate\Support\Facades\DB;

//use App\Models\UserTeam;

class TeamService extends AbstractService implements ITeamService
{
    public function __construct(ITeamRepository $repository, UserTeamService $userTeamService, UserService $userService, IUserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->userTeamService = $userTeamService;
        $this->userService = $userService;
    }

    public function getAllMember($id)
    {
        $members = $this->userTeamService->getMemberTeamAttribute($id);
        $membersWithName = [];
        foreach ($members as $member) {
            $member->name = $this->userRepository->getMemberName($member->user_id);
            $membersWithName[] = $member;
        }
        return $membersWithName;
    }

    public function getUsersAttribute($id)
    {
        return $this->userRepository->getModel()->where('id', $id)->first()->name;
    }

    public function save(array $options = [])
    {
        DB::beginTransaction();
        parent::save($options); // TODO: Change the autogenerated stub
        $newest_team = $this->model->orderBy('created_at', 'desc')->first();

        $newUserTeam = new $this->userTeamRepository;
        $newUserTeam->team_id = $newest_team->id;
        $newUserTeam->user_id = $newest_team->leader_id;
        $newUserTeam->save();
        DB::commit();
    }
}
