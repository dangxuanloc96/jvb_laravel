<?php
/**
 * GroupService class
 * Author: jvb
 * Date: 2019/05/16 14:31
 */

namespace App\Services;

use App\Models\Group;
use App\Models\LaborCalendar;
use App\Models\LaborCalendarDetail;
use App\Models\User;
use App\Services\Contracts\ILaborCalendarService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\Factory;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class LaborCalendarService extends AbstractService implements ILaborCalendarService
{
    public function saveLaborCalendar($month,$year,$listUserID,$content)
    {
            $this->checkExistLaborMonth($month,$year);
            $day = Carbon::parse($month.'/1/'.$year);
            $dayOfMonth = $day->daysInMonth;
            $arrayMales = [];
            $arrayFemales = [];

            foreach ($listUserID as $id)
            {
                $user = User::find($id);
                if($user->sex == SEX['male'])
                {
                    $arrayMales [] = $id;
                }else{
                    $arrayFemales [] = $id;
                }
            }

            if(empty($arrayMales))
            {
                $this->onlyGender($day,$dayOfMonth,$arrayFemales,$content);
            }elseif(empty($arrayFemales))
            {
                $this->onlyGender($day,$dayOfMonth,$arrayMales,$content);
            }else{
                for ($i = 1; $i <= $dayOfMonth ; $i++)
                {
                    //only apply for two user in day
                    if($day->dayOfWeek != DAY_OF_WEEK['sunday'] && $day->dayOfWeek != DAY_OF_WEEK['saturday'])
                    {
                        if(empty($arrayMales)){
                            $this->onlyGender($day,$dayOfMonth,$arrayFemales,$content);
                            break;
                        }elseif(empty($arrayFemales)){
                            $this->onlyGender($day,$dayOfMonth,$arrayMales,$content);
                            break;
                        }else{
                            $laborID = LaborCalendar::create([
                                'creator_id' => Auth::guard('admin')->user()->id,
                                'content' => $content,
                                'status' => LABOR_STATUS['default'],
                                'labor_date' => $day->toDateTimeString()
                            ])->id;

                            $randomIndexMales = rand(0,count($arrayMales) -1);
                            LaborCalendarDetail::create([
                                'user_id' => $arrayMales[$randomIndexMales],
                                'labor_calendar_id' => $laborID,
                            ]);
                            unset($arrayMales[$randomIndexMales]);
                            $arrayMales = array_values($arrayMales);

                            $randomIndexFemales = rand(0,count($arrayFemales) -1);
                            LaborCalendarDetail::create([
                                'user_id' => $arrayFemales[$randomIndexFemales],
                                'labor_calendar_id' => $laborID,
                            ]);
                            unset($arrayFemales[$randomIndexFemales]);
                            $arrayFemales = array_values($arrayFemales);
                        }
                    }
                    $day->addDay(1);
                }
            }
        return;
    }

    public function onlyGender($day,$dayOfMonth,$arrayID,$content)
    {
        for ($i = 1 ; $i <= $dayOfMonth ; $i++)
        {
            if($day->dayOfWeek != DAY_OF_WEEK['sunday'] && $day->dayOfWeek != DAY_OF_WEEK['saturday'])
            {
                if(!empty($arrayID))
                {
                    $laborID = LaborCalendar::create([
                        'creator_id' => Auth::guard('admin')->user()->id,
                        'content' => $content,
                        'status' => LABOR_STATUS['default'],
                        'labor_date' => $day->toDateTimeString()
                    ])->id;

                    For($j = 0 ; $j < NUMBER_OF_USER_LABOR_IN_DAY; $j++)
                    {
                        $indexRandom = rand(0,count($arrayID)-1);
                        LaborCalendarDetail::create(
                            [
                                'user_id' => $arrayID[$indexRandom],
                                'labor_calendar_id' => $laborID
                            ]
                        );
                        unset($arrayID[$indexRandom]);
                        $arrayID = array_values($arrayID);
                    }
                }else{
                    break;
                }
            }
            $day->addDays(1);
        }
    }

    public function getLaborCalendar($searchMonth,$searchYear)
    {
        $laborCalendars = [];
        $startDayOfMonth = Carbon::parse($searchMonth.'/1/'.$searchYear)->firstOfMonth()->toDateString();
        $endDayOfMonth = Carbon::parse($searchMonth.'/1/'.$searchYear)->endOfMonth()->toDateString();
        $listLaborCalendar = LaborCalendar::where('labor_date','>=',$startDayOfMonth)
            ->where('labor_date','<=',$endDayOfMonth)
            ->get();

        foreach ($listLaborCalendar as $LaborCalendar)
        {
            foreach ($LaborCalendar->laborCalendarDetail as $detail)
            {
                $laborCalendar = [];
                $laborCalendar['title'] = User::find($detail->user_id)->name;
                $laborCalendar['start_date'] = $LaborCalendar->labor_date;
                $laborCalendar['status'] = $LaborCalendar->status;
                $laborCalendar['group_id'] = $LaborCalendar->id;
                $laborCalendar['url'] = route('admin::labor_calendar.edit',$LaborCalendar->id);

                $laborCalendars [] = $laborCalendar;
            }
        }

        return $laborCalendars;
    }

    public function getInfoLaborCalendar($laborCalendarID)
    {
        $laborCalendar = LaborCalendar::find($laborCalendarID);
        return $laborCalendar;
    }

    public function checkExistLaborMonth ($month,$year)
    {
        $startDay = Carbon::parse($month.'/1/'.$year)->firstOfMonth()->toDateString();
        $endDay = Carbon::parse($month.'/1/'.$year)->lastOfMonth()->toDateString();

        $oldLaborList = LaborCalendar::where('labor_date','>=',$startDay)->where('labor_date','<=',$endDay)->get();
        if(!$oldLaborList->isEmpty())
        {
            foreach ($oldLaborList as $labor)
            {
                foreach ($labor->laborCalendarDetail as $laborDetail)
                {
                    $laborDetail->delete();
                }
                $labor->delete();
            }
        }

    }

}
