<?php
/**
 * ReportService class
 * Author: jvb
 * Date: 2019/01/21 03:42
 */

namespace App\Services;

use App\Events\ReportCreatedNoticeEvent;
use App\Models\Config;
use App\Models\Group;
use App\Models\Report;
use App\Models\ReportReceiver;
use App\Models\ReportReply;
use App\Models\User;
use App\Notifications\SentReport;
use App\Repositories\Contracts\IReportRepository;
use App\Services\Contracts\IReportService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportService extends AbstractService implements IReportService
{
    /**
     * ReportService constructor.
     *
     * @param \App\Models\Report                            $model
     * @param \App\Repositories\Contracts\IReportRepository $repository
     */
    public function __construct(Report $model, IReportRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     * @param int     $teamId
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search, &$teamId)
    {
        if (!$request->has('year'))
            $request->merge(['year' => date('Y')]);
        if (!$request->has('month'))
            $request->merge(['month' => date('n')]);

        if (!$request->has('team_id')) {
            $user = Auth::user();
            $team = $user->team();
            if ($team) {
                $teamId = $team->id;

                if (!$request->has('type')) {
                    $request->merge(['type' => 2]);
                    $request->merge(['team_id' => $teamId]);
                }

                if ($request->type == 2 && !$request->has('team_id')) {
                    $request->merge(['team_id' => $teamId]);
                }
            } else {
                $teamId = 0;
            }

        } else {
            $teamId = $request->get('team_id');
        }

        $criterias = $request->only('page', 'page_size', 'search', 'type', 'date_from', 'date_to', 'year', 'month', 'team_id');
        $currentUser = Auth::user();

        $perPage = $criterias['page_size'] ?? REPORT_PAGE_SIZE;
        $search = $criterias['search'] ?? '';
        $model = $this->model
            ->select([
                'id',
                'user_id',
                'week_num',
                'to_ids',
                'title',
                'status',
                'content',
                'report_type',
                'report_date',
                'color_tag',
                'reports.created_at',
                'updated_at',
            ])
            ->where(function ($q) use ($currentUser) {
                $q->where('status', ACTIVE_STATUS)
                    ->orWhere(function ($p) use ($currentUser) {
                        $p->where('status', REPORT_DRAFT)->where('user_id', $currentUser->id);
                    });
            })
            ->search($search)
            ->with(['receivers' => function ($q) {
                $q->select('users.id', 'users.name', 'avatar')->orderBy('jobtitle_id', 'desc');
            }, 'reportReplies' => function ($q) {
                $q->orderBy('report_reply.id', 'desc');
            }])
            ->withCount('reportReplies')
            ->orderBy('id', 'desc');

        if (isset($criterias['date_from'])) {
            $model->where('created_at', '>=', $criterias['date_from']);
        }
        if (isset($criterias['date_to'])) {
            $model->where('created_at', '<=', $criterias['date_to']);
        }
        if (!empty($criterias['year'])) {
            $model->where('year', $criterias['year']);
        }
        if (!empty($criterias['month'])) {
            $model->where('month', $criterias['month']);
        }
        $model->where(function ($modelInner) use ($request, $criterias, $currentUser) {
            $type = $request->get('type');
            if (isset($type)) {
                if ($type == REPORT_SEARCH_TYPE['private']) {
                    $modelInner->where('user_id', Auth::id());
                } elseif ($type == REPORT_SEARCH_TYPE['team']) {
                    if (isset($criterias['team_id'])) {
                        $modelInner->where('team_id', $criterias['team_id']);
                    }
                }
            }
            if ($type != REPORT_SEARCH_TYPE['private']) {
                if ($currentUser->isMaster()) {
                } else if ($currentUser->isGroupManager()) {
                    $groupManage = Group::where('manager_id', $currentUser->id)->first();
                    if ($groupManage) {
                        $groupId = $groupManage->id;

                        $modelInner->where(function ($q) use ($groupId) {
                            $q->where('is_private', REPORT_PUBLISH)->orWhere('group_id', $groupId);
                        });
                    }
                } else {
                    $team = $currentUser->team();
                    if ($team) {
                        $modelInner->where(function ($q) use ($type, $team, $currentUser) {
                            $q->where('is_private', REPORT_PUBLISH)
                                ->orWhere(function ($p) use ($team) {
                                    $p->where('is_private', REPORT_PRIVATE)
                                        ->where('team_id', $team->id);
                                });

                            if ($type == REPORT_SEARCH_TYPE['all']) {
                                $q->orWhere(function ($p) use ($currentUser) {
                                    $p->where('user_id', $currentUser->id);
                                });
                            }
                        });
                    } else {
                        $modelInner->where('is_private', REPORT_PUBLISH);
                    }
                }
            }
            if (!$currentUser->isMaster()) {
                if (!($type == REPORT_SEARCH_TYPE['team'] && isset($criterias['team_id']))) {
                    $modelInner->orWhereHas('reportReceivers', function ($q) use ($currentUser) {
                        $q->where('user_id', $currentUser->id);
                    });
                }
            }
        });
        return $model->paginate($perPage);
    }

    public function create(Request $request)
    {
        $data = $request->only('status', 'choose_week', 'to_ids', 'content', 'is_new', 'is_private');
        $choose_week = $data['choose_week'];
        $reportType = REPORT_TYPE_WEEKLY;
        $reportDate = null;
        if ($choose_week == 0 || $choose_week == 1) {
            get_week_info($choose_week, $week_number);
        } else {
            $reportDate = date_create_from_format('d/m', $data['choose_week']);

            $choose_week = 0;
            $reportType = REPORT_TYPE_DAILY;
            $week_number = get_week_number($choose_week);
        }

        $data['title'] = $this->getReportTitle($data['choose_week']);

        $data['week_num'] = $week_number;
        $data['year'] = date('Y');
        $data['user_id'] = Auth::id();
        $data['month'] = getMonthFormWeek($week_number);
        $data['report_date'] = $reportDate;

        $report = $this->getDraftReport();
        DB::beginTransaction();
        if ($report) {
            $report->fill($data);
            $report->save();
        } else {
            $report = Report::where([
                'user_id' => Auth::id(),
                'year' => date('Y'),
                'month' => $data['month'],
                'week_num' => $week_number,
                'report_type' => $reportType,
                'report_date' => $reportDate,
            ])->first();

            if (!$report) {
                $user = Auth::user();
                $team = $user->team();
                if ($team)
                    $data['color_tag'] = $team->color;

                $data['report_type'] = $reportType;
                $report = new Report($data);
                $report->save();
                $user->notify(new SentReport($report));
            } else {
                $report->fill($data);
                $report->save();
            }
        }

        //make receivers
        $receivers = User::whereIn('id', $data['to_ids'])->get();
        ReportReceiver::where('report_id', $report->id)->delete();
        foreach ($receivers as $receiver) {
            $dataReceivers[] = [
                'report_id' => $report->id,
                'user_id' => $receiver->id,
            ];
            broadcast(new ReportCreatedNoticeEvent($report, $receiver))->toOthers();
        }
        ReportReceiver::insertAll($dataReceivers);

        DB::commit();

        return $report;
    }

    /**
     * @return array
     */
    public function getReportReceiver()
    {
        $user= Auth::user();
        $masters = [
            'Ban giám đốc' => $this->getUserModel(['jobtitle_id' => MASTER_ROLE])->toArray()
        ];
        $managerUsers = $this->getUserModel(['jobtitle_id' => MANAGER_ROLE], false);
        $managers = [
            'Manager' => $managerUsers->get()->toArray()
        ];

        if ($user->jobtitle_id >= MANAGER_ROLE) {
            return $masters;
        } else if ($user->jobtitle_id == TEAMLEADER_ROLE) {
            return $managers + $masters;
        } else {
            $team = $user->team();
            if ($team) {
                //team leader
                $userIds = [
                    $team->leader_id,
                    $team->group->manager_id ?? 0,
                ];
                $directs = [
                    'Quản lý trực tiếp' => $this->getUserModel([], false)->whereIn('id', $userIds)->get()->toArray()
                ];
                //manager
                $otherManagers = $managerUsers->whereNotIn('id', $userIds)->get();
                $managers = [
                    'Manager' => $otherManagers->toArray()
                ];
                //other
                $others = $this->getUserModel([], false)->where('jobtitle_id', '!=', MASTER_ROLE)->whereNotIn('id', $userIds + $otherManagers->pluck('id')->toArray())->orderBy('name')->get()->toArray();
                $users = [
                    'Khác' => $others
                ];
                return $directs + $managers + $masters + $users;
            } else {
                // manager + team leader
                $teamLeadUsers = $this->getUserModel(['jobtitle_id' => TEAMLEADER_ROLE]);
                $teamLeads = [
                    'Team Leader' => $teamLeadUsers->toArray()
                ];
                //other
                $others = $this->getUserModel([], false)->where('jobtitle_id', '=', STAFF_ROLE)->whereNotIn('id', [$user->id])->orderBy('name')->get()->toArray();
                $users = [
                    'Khác' => $others
                ];
                return $teamLeads + $managers + $masters + $users;
            }
        }
    }

    private function getUserModel($conditions = [], $isGet = true)
    {
        $model = User::select('id', 'staff_code', 'name', 'avatar')
            ->where('id', '!=', Auth::id())
            ->where('contract_type', STAFF_CONTRACT_TYPES)->where('status', ACTIVE_STATUS)->where($conditions);

        if ($isGet) {
            return $model->get();

        } else {
            return $model;
        }
    }

    /**
     * @param int $id
     *
     * @return Report
     */
    public function detail($id)
    {
        $report = $this->model->where([
            'id' => $id,
            'status' => ACTIVE_STATUS
        ])->with('user:id,name,email,avatar')->first();

        return $report;
    }

    /**
     * @return Report
     */
    public function newReportFromTemplate()
    {
        $report = new Report();
        $report->is_new = true;

        //Fill data from template
        $config = Config::firstOrNew(['id' => 1]);

        $report->title = $this->generateTitle($config->weekly_report_title);

        $report->content = $config->html_weekly_report_template;

        return $report;
    }

    /**
     * @param int $type : -1: daily report; 0:
     *
     * @return mixed
     */
    public function getReportTitle($type)
    {
        if ($type == 0 || $type == 1) {
            $config = Config::firstOrNew(['id' => 1]);

            $template = $config->weekly_report_title;

            return $this->generateTitle($template, $type);
        } else {
            return "Báo cáo ngày [" . $type . "]: " . Auth::user()->name;
        }
    }

    /**
     * @return Report
     */
    public function getFromDraftOrTemplate()
    {
        $report = $this->getDraftReport();

        if (!$report)
            $report = $this->newReportFromTemplate();

        return $report;
    }

    /**
     * @return Report
     */
    public function getDraftReport()
    {
        return Report::where([
            'user_id' => Auth::id(),
            'status' => REPORT_DRAFT
        ])->orderBy('id', 'desc')->first();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function replyReport(Request $request)
    {
        $data = $request->only('content', 'report_id');
        $data['user_id'] = Auth::id();

        $reportReply = new ReportReply($data);
        $reportReply->save();
        /** @var NotificationService $notificationService */
        $notificationService = app()->make(NotificationService::class);
        $notificationService->sentReportNotification($request->get('report_id'), Auth::user(), $request->get('content'));

        return isset($reportReply);
    }

    /**
     * @param $template
     *
     * @return mixed
     */
    private function generateTitle($template, $type = 0)
    {
        [$firstDay, $lastDay] = get_first_last_day_in_week($type, $day);

        'Báo cáo tuần ' . get_week_info($type, $week_number);
        //1. ${staff_name}
        $result = str_replace('${staff_name}', Auth::user()->name, $template);

        //2. ${week_number}
        $result = str_replace('${week_number}', $week_number, $result);

        //3. ${d}
        $result = str_replace('${d}', date('d'), $result);

        //4. ${m}
        $result = str_replace('${m}', date('m'), $result);

        //5. ${Y}
        $result = str_replace('${Y}', date('Y'), $result);

        //6. ${first_day}
        $result = str_replace('${first_day}', $firstDay, $result);

        //7. ${last_day}
        $result = str_replace('${last_day}', $lastDay, $result);

        return $result;

    }
}
