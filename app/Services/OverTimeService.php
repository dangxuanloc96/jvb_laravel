<?php
/**
 * OverTimeService class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */

namespace App\Services;

use App\Models\OverTime;
use App\Models\OverTimesExplanation;
use App\Repositories\Contracts\IOverTimeRepository;
use App\Services\Contracts\IOverTimeService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class OverTimeService extends AbstractService implements IOverTimeService
{
    /**
     * OverTimeService constructor.
     *
     * @param \App\Models\OverTime                            $model
     * @param \App\Repositories\Contracts\IOverTimeRepository $repository
     */
    public function __construct(OverTime $model, IOverTimeRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }


    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search)
    {
        $model = $this->getSearchModel($request);
        return $model->search($search)->paginate($perPage);
    }

    /**
     * @param Request $request
     * @param         $search
     *
     * @return mixed
     */
    public function export(Request $request)
    {
        $search = $request->search;
        $model = $this->getSearchModel($request, true);
        return $model->search($search)->get();
    }

    protected function getSearchModel(Request $request, $forExport = false)
    {
        $model = $this->model;
        $year = $request->get('year');

        if ($year) {
            $model = $model->whereYear('work_day', $year);
        }
        $month = $request->get('month');
        if ($month) {
            $model = $model->whereMonth('work_day', $month);
        }
        $work_day = $request->get('work_day');
        if ($work_day) {
            $model = $model->whereDate('work_day', $work_day);
        }
        $type = $request->get('type');
        if ($type != null) {
            $model = $model->where('ot_type', $type);
        }
        $status = $request->get('status');
        if ($status != null) {
            $model = $model->where('ot_times.status', $status);
        }
        $userId = $request->get('user_id');
        if ($userId) {
            $model = $model->where('creator_id', $userId);
        }

        if ($forExport) {
            $model->orderBy('creator_id')->orderBy('work_day');
        } else if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('work_day', 'desc')->orderBy('creator_id');
        }

        return $model;
    }

}
