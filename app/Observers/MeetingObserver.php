<?php

namespace App\Observers;

use App\Models\Meeting;
use App\Services\Contracts\IMeetingService;
use App\Services\MeetingService;
use Auth;

/**
 * @property IMeetingService service
 */
class MeetingObserver
{
    public function __construct()
    {
        $this->service = app()->make(MeetingService::class);
//        $this->reminder_repository = $reminder_repository;
//        $this->reminder_member_repository = $reminder_member_repository;
    }

    /**
     * Handle the Meeting "created" Meeting.
     *
     * @param Meeting $meeting
     *
     * @return void
     */
    public function created(Meeting $meeting)
    {
        if ($meeting) {
//            $route_detail_reminder = 'meetings';
//            $carbon_date_time = Carbon::now();
//            $data = [
//                'name' => $meeting->title,
//                'content' => $meeting->content,
//                'start_date' => $carbon_date_time->toDateString(),
//                'start_loop' => $carbon_date_time->toTimeString(),
//                'end_loop' => $meeting->end_time,
//                'option_loop' => REMINDER_MEETING_OPTION_LOOP,
//                'route_detail_reminder' => $route_detail_reminder,
//                'meeting_id' => $meeting->id,
//            ];
//            $reminder = $this->reminder_repository->save($data);
//            $reminder_id = $reminder->id;
//            if($meeting->participants != null) {
//                $user_reminder = $meeting->participants;
//                array_push($user_reminder, Auth::id());
//                foreach ($user_reminder as $item) {
//                    if(isset($item)) {
//                        $newReminder = new ReminderMember();
//                        $newReminder->reminder_id = $reminder_id;
//                        $newReminder->user_id = $item;
//                        $newReminder->save();
//                    }
//                }
//            }
            $this->service->sendMeetingNotice($meeting, 0);
        }
    }

    /**
     * Handle the Meeting "updated" Meeting.
     *
     * @param Meeting $meeting
     *
     * @return void
     */
    public function updated(Meeting $meeting)
    {
        if ($meeting) {
            //update reminder
//            $carbon_date_time = Carbon::now();
//            $reminder = Reminder::where('meeting_id', $meeting->id)
//            ->update([
//                'name' => $meeting->title,
//                'content' => $meeting->content,
//                'start_date' => $carbon_date_time->toDateString(),
//                'start_loop' => $carbon_date_time->toTimeString(),
//                'end_loop' => $meeting->end_time,
//                'meeting_id' => $meeting->id,
//            ]);
//
//            $reminder_id = Reminder::select('id')->where('meeting_id', $meeting->id)->get();
//            if($meeting->participants != null) {
//                $user_reminder = $meeting->participants;
//                foreach ($user_reminder as $item) {
//                    ReminderMember::where('reminder_id', $reminder_id)
//                    ->update([
//                        'user_id' => $item,
//                        'reminder_id' =>$reminder_id,
//                    ]);
//                }
//            }
            //update reminder
            $this->service->sendMeetingNotice($meeting, 3);
        }
    }

    /**
     * Handle the meeting "deleted" meeting.
     *
     * @param Meeting $meeting
     *
     * @return void
     */
    public function deleted(Meeting $meeting)
    {
        if ($meeting) {
//            $reminder = Reminder::where('meeting_id', '=', $meeting->id)
//            ->update(['meeting_deleted' => 1]);
            $this->service->sendMeetingNotice($meeting, 2);
        }
    }

}
