<?php

namespace App\Policies;

use App\Models\OverTime;
use Illuminate\Auth\Access\HandlesAuthorization;

class OverTimePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function approverOt($user, OverTime $overTime)
    {
        if ($user->jobtitle_id >= MANAGER_ROLE || ($overTime->assign_id == $user->id)) {
            return true;
        }
        return null;
    }
}
