<?php

namespace App\Http\Middleware;

use App\Models\WorkTime;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class ActivityHistoryUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->is_remote_checkin === IS_REMOTE_STAFF) {
                $now = Carbon::now();
                $date = $now->format('Y-m-d');
                if (!WorkTime::where([
                    'user_id' => $user->id,
                    'work_day' => $date])->first()) {
                    $workTime = new WorkTime([
                        'user_id' => $user->id,
                        'work_day' => $date,
                        'start_at' => $now->format('H:i:s'),
                    ]);
                    $workTime->save();
                }
            }

            Auth::user()->update([
                'last_activity_at' => Carbon::now()
            ]);
        }
        return $next($request);
    }
}
