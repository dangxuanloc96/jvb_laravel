<?php

namespace App\Http\Middleware;

use App\Facades\AuthAdmin;
use App\Models\AdminLog;
use Closure;

class AdminActivityLog
{
    const EXPEPT_PARAMS = [
        'token',
        '_token',
        '_method',
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->isMethod('get')) {
            $data = $this->trimJsonLog($request->except(self::EXPEPT_PARAMS));
            $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS);

            $log = new AdminLog([
                'user_id' => AuthAdmin::id(),
                'method' => $request->method(),
                'path' => $request->path(),
                'json_log' => $json,
            ]);

            $log->save();
        }
        return $next($request);
    }

    private function trimJsonLog($arrData)
    {
        return array_map(function ($item) {
            if (is_array($item)) {
                return $this->trimJsonLog($item);
            } else if (is_string($item)) {
                return str_limit(strip_tags(nl2br($item)), 50);
            } else {
                return '...';
            }
        }, $arrData);
    }

}
