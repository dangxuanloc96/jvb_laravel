<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class ReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:reminders',
            'start_date' => 'required|date',
            'content' => 'required',
            'start_loop' => 'required',
            'end_loop' => 'required',
            'user_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'tên',
            'start_date' => 'thời gian lặp',
            'content' => 'nội dung',
            'start_loop' => 'giờ bắt đầu',
            'end_loop' => 'giờ kết thúc',
            'user_id' => 'người tham gia',
        ];
    }
}
