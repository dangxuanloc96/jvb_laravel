<?php

namespace App\Http\Controllers\Api;

use App\Services\ShareService;
use App\Transformers\ShareDocumentTransformer;
use App\Transformers\ShareExperienceTransformer;
use App\Transformers\ShareTransformer;
use Illuminate\Http\Request;
use Illuminate\View\View as ViewAlias;

class ShareController extends ApiBaseController
{

    /**
     * @var ShareService
     */
    private $shareService;

    /**
     * @var ShareExperienceTransformer
     */
    private $experienceTransformer;

    /**
     * @var ShareDocumentTransformer
     */
    private $documentTransformer;

    /**
     * ShareController constructor.
     *
     * @param ShareService             $shareService
     * @param ShareDocumentTransformer $transformer
     */
    public function __construct(
        ShareService $shareService,
        ShareTransformer $transformer,
        ShareDocumentTransformer $documentTransformer,
        ShareExperienceTransformer $experienceTransformer
    )
    {
        $this->shareService = $shareService;
        $this->documentTransformer = $documentTransformer;
        $this->experienceTransformer = $experienceTransformer;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /share/document
     * @apiName          List document share
     * @apiDescription   Sharing document list
     * @apiGroup         Share
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search shares.
     *
     * @apiSuccess {Object[]} shares List of share.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|ViewAlias
     */
    public function document(Request $request)
    {
        $shares = $this->shareService->documentList($request, $perPage, $search);
        return $this->respondTransformer($shares, $this->documentTransformer, 'documents');
    }

    /**
     * @api              {get} /share/experience
     * @apiName          List experience share
     * @apiDescription   Sharing experience list
     * @apiGroup         Share
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search shares.
     *
     * @apiSuccess {Object[]} shares List of share.
     * @apiSuccess {Object} meta Meta Pagination.
     */

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|ViewAlias
     */
    public function experience(Request $request)
    {
        $shares = $this->shareService->documentList($request, $perPage, $search);
        return $this->respondTransformer($shares, $this->experienceTransformer, 'experiences');
    }

    /**
     * @api              {get} /shares/:id
     * @apiName          Share detail
     * @apiDescription   GetShare
     * @apiGroup         Share
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} share Share info.
     */
    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|ViewAlias
     */
    public function detail($id)
    {
        $share = $this->shareService->detail($id);
        if ($share != null) {
            return $this->respondTransformer($share);
        }
        return $this->respondNotfound();
    }

}
