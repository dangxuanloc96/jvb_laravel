<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\IUserService;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

/**
 * UserController
 * Author: jvb
 * Date: 2018/07/16 10:34
 */
class UserController extends ApiBaseController
{

    /**
     * UserController constructor.
     *
     * @param \App\Repositories\Contracts\IUserRepository $repository
     * @param \App\Transformers\UserTransformer           $transformer
     */
    public function __construct(
        IUserRepository $repository,
        IUserService $service,
        UserTransformer $transformer
    )
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /users
     * @apiName          UserList
     * @apiDescription   User List
     * @apiGroup         User
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search users.
     *
     * @apiSuccess {Object[]} users List of user.
     * @apiSuccess {Object} meta Meta Pagination.
     */

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $collections = $this->service->getContact($request, $perPage, $search, false);
        return $this->respondTransformer($collections);
    }

    /**
     * @api              {get} /users/:id
     * @apiName          UserDetail
     * @apiDescription   User detail
     * @apiGroup         User
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} user User info.
     */

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $report = $this->service->detail($id);
        if ($report != null) {
            return $this->respondTransformer($report);
        }
        return $this->respondNotfound();
    }

}
