<?php

namespace App\Http\Controllers\Api;

use App\Services\Contracts\IPunishesService;
use App\Transformers\PunishesTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PunishController extends ApiBaseController
{

    /**
     * @var IPunishesService
     */
    private $punishesService;

    /**
     * PunishesController constructor.
     *
     * @param IPunishesService    $punishesService
     * @param PunishesTransformer $transformer
     */
    public function __construct(
        IPunishesService $punishesService,
        PunishesTransformer $transformer
    )
    {
        $this->punishesService = $punishesService;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /punishes
     * @apiName          List punish
     * @apiDescription   PunishList
     * @apiGroup         Punish
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search punishes.
     *
     * @apiSuccess {Object[]} punishes List of punish.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $punishess = $this->punishesService->search($request, Auth::id(), $perPage, $search);
        return $this->respondTransformer($punishess);
    }

    /**
     * @api              {get} /punishes/:id
     * @apiName          Punish detail
     * @apiDescription   GetPunish
     * @apiGroup         Punish
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} punish punish info.
     */
    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $punishes = $this->punishesService->detail($id);
        if ($punishes != null) {
            return $this->respondTransformer($punishes);
        }
        return $this->respondNotfound();
    }

}
