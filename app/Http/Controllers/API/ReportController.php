<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\ReplyReportRequest;
use App\Models\Report;
use App\Services\Contracts\IReportService;
use App\Transformers\ReportTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends ApiBaseController
{
    /**
     * ReportController constructor.
     *
     * @param IReportService    $service
     * @param ReportTransformer $transformer
     */
    public function __construct(
        IReportService $service,
        ReportTransformer $transformer
    )
    {
        $this->service = $service;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /reports
     * @apiName          List report
     * @apiDescription   ReportList
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search reports.
     *
     * @apiSuccess {Object[]} reports List of report.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    public function index(Request $request)
    {
        $reports = $this->service->search($request, $perPage, $search, $teamId);
        return $this->respondTransformer($reports);
    }

    /**
     * @api              {get} /reports/:id
     * @apiName          Report detail
     * @apiDescription   GetReport
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} report Report info.
     */
    public function detail($id)
    {
        $report = $this->service->detail($id);
        if ($report != null) {
            return $this->respondTransformer($report);
        }
        return $this->respondNotfound();
    }

    /**
     * @api              {get} /report-receivers
     * @apiName          Report receivers
     * @apiDescription   GetReportReceiver
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {List} list receiver.
     */
    public function getReportReceiver()
    {
        $receivers = $this->service->getReportReceiver();
        return $this->respond($receivers);
    }

    /**
     * @api              {get} /new-report
     * @apiName          New report
     * @apiDescription   GetNewReport
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Report} Report.
     */
    public function create()
    {
        $report = $this->service->getFromDraftOrTemplate();
        return $this->respondTransformer($report);
    }

    /**
     * @api              {get} /report
     * @apiName          Report detail
     * @apiDescription   ReportDetail
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {Number} id Report Id.
     *
     * @apiSuccess {Report} Report.
     */
    public function getReport(Request $request)
    {
        return $this->respond([
            'report' => $this->service->detail($request->get('id'))
        ]);
    }

    /**
     * @api              {post} /report
     * @apiName          Report create
     * @apiDescription   SaveReport
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {Number} choose_week Week number.
     * @apiParam {Number} status Save draft or final.
     * @apiParam {Array} to_ids Receiver Ids.
     * @apiParam {String} content Report content.
     *
     * @apiSuccess {Object} success.
     */
    public function saveReport(CreateReportRequest $request)
    {
        $this->service->create($request);
        return $this->responseSuccess();
    }

    /**
     * @api              {post} /reply-report
     * @apiName          Report reply
     * @apiDescription   ReportReply
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {Number} report_id Report Id.
     * @apiParam {String} content Report content.
     *
     * @apiSuccess {Object} success.
     */
    public function replyReport(ReplyReportRequest $request)
    {
        $this->service->replyReport($request);
        return $this->responseSuccess();
    }

    /**
     * @api              {delete} /report/:id
     * @apiName          Report remove
     * @apiDescription   ReportRemove
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} success.
     */
    public function deleteReport($id)
    {
        $report = Report::find($id);
        if ($report && Auth::user()->can('delete', $report)) {
            $report->delete();
            $this->responseSuccess();
        }

        $this->responseFail();
    }

}
