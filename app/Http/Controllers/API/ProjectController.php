<?php

namespace App\Http\Controllers\Api;

use App\Services\Contracts\IProjectService;
use App\Transformers\ProjectTransformer;
use Illuminate\Http\Request;

class ProjectController extends ApiBaseController
{

    /**
     * @var IProjectService
     */
    private $projectService;

    /**
     * ProjectController constructor.
     *
     * @param IProjectService    $projectService
     * @param ProjectTransformer $transformer
     */
    public function __construct(
        IProjectService $projectService,
        ProjectTransformer $transformer
    )
    {
        $this->projectService = $projectService;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /projects
     * @apiName          List project
     * @apiDescription   ProjectList
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search projects.
     *
     * @apiSuccess {Object[]} projects List of project.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $projects = $this->projectService->search($request, $perPage, $search);
        return $this->respondTransformer($projects);
    }

    /**
     * @api              {get} /projects/:id
     * @apiName          ProjectDetail
     * @apiDescription   GetProject
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $project = $this->projectService->detail($id);
        if ($project != null) {
            return $this->respondTransformer($project);
        }
        return $this->respondNotfound();
    }

}
