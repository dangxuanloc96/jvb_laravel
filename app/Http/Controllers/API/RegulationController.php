<?php

namespace App\Http\Controllers\Api;

use App\Services\Contracts\IRegulationService;
use App\Transformers\RegulationTransformer;
use Illuminate\Http\Request;

class RegulationController extends ApiBaseController
{

    /**
     * @var IRegulationService
     */
    private $regulationService;

    /**
     * RegulationController constructor.
     *
     * @param IRegulationService    $regulationService
     * @param RegulationTransformer $transformer
     */
    public function __construct(
        IRegulationService $regulationService,
        RegulationTransformer $transformer
    )
    {
        $this->regulationService = $regulationService;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /regulations
     * @apiName          List regulation
     * @apiDescription   RegulationList
     * @apiGroup         Regulation
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search regulations.
     *
     * @apiSuccess {Object[]} regulations List of regulation.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $regulations = $this->regulationService->search($request, $perPage, $search);
        return $this->respondTransformer($regulations);
    }

    /**
     * @api              {get} /regulations/:id
     * @apiName          Regulation detail
     * @apiDescription   GetRegulation
     * @apiGroup         Regulation
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} regulation Regulation info.
     */
    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $regulation = $this->regulationService->detail($id);
        if ($regulation != null) {
            return $this->respondTransformer($regulation);
        }
        return $this->respondNotfound();
    }

}
