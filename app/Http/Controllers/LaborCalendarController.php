<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\ILaborCalendarRepository;
use App\Services\Contracts\ILaborCalendarService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaborCalendarController extends Controller
{
    public $repository;
    public $service;
    public function __construct(ILaborCalendarRepository $repository,ILaborCalendarService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $month = Carbon::now()->month;
        if($request->has('month'))
        {
            $month = $request->get('month');
        }
        $listLaborCalendars = $this->repository->getListLaborCalendar($month);
        $timeSearch = Carbon::parse(Carbon::now()->year.'/'.$month.'/01');
        $workContent = $this->repository->getWorkContent($month);
        return view('end_user.labor_calendar.index',compact('month','listLaborCalendars','timeSearch','workContent'));
    }
}
