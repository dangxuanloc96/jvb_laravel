<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdditionalDate;
use App\Models\CalendarOff;
use App\Models\Config;
use App\Models\LaborCalendar;
use App\Repositories\Contracts\ILaborCalendarRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\ILaborCalendarService;
use App\Services\Contracts\IUserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Helpers\DateTimeHelper;
use Illuminate\Support\Facades\Log;

class LaborCalendarController extends AdminBaseController
{
    protected $resourceAlias = 'admin.labor_calendar';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::labor_calendar';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = LaborCalendar::class;

    protected $resourceTitle = 'Lịch trực nhật';

    protected $resourceSearchExtend = 'admin.labor_calendar._search_extend';

    protected $userRepository;

    protected $service;

    public $repository;

    public function __construct(IUserRepository $userRepository,ILaborCalendarService $service,ILaborCalendarRepository $repository)
    {
        $this->userRepository = $userRepository;
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $searchMonth = Carbon::now()->month;
        $searchYear = Carbon::now()->year;
        $nowDate = Carbon::now()->toDateString();
        if($request->has('month') || $request->has('year'))
        {
            $searchMonth = $request->get('month');
            $searchYear = $request->get('year');
            $nowDate = Carbon::parse($searchYear.'/'.$searchMonth.'/1');
        }

        $laborCalendars = $this->service->getLaborCalendar($searchMonth,$searchYear);
        return view($this->resourceAlias . '._layout', [
            'resourceAlias' => $this->resourceAlias,
            'resourceSearchExtend' => $this->resourceSearchExtend,
            'resourceRoutesAlias' => $this->resourceRoutesAlias,
            'resourceTitle' => $this->resourceTitle,
            'searchMonth' => $searchMonth,
            'searchYear' => $searchYear,
            'laborCalendars' => $laborCalendars,
            'nowDate' => $nowDate
        ]);
    }

    public function create(Request $request)
    {
        $laborUserList = $this->userRepository->getLaborUserList();
        $listBeforeSixMonth = DateTimeHelper::getBeforeSixMonth();
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;
        $countUserChose = DateTimeHelper::countUserChose($currentMonth,$currentYear);
        return view($this->resourceAlias . '.create', [
            'resourceAlias' => $this->resourceAlias,
            'resourceSearchExtend' => $this->resourceSearchExtend,
            'resourceRoutesAlias' => $this->resourceRoutesAlias,
            'resourceTitle' => $this->resourceTitle,
            'laborUserList' => $laborUserList,
            'listBeforeSixMonth' => $listBeforeSixMonth,
            'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
            'countUserChose' => $countUserChose
        ]);
    }

    public function countUserChose()
    {
        $month = request()->get('month');
        $year = request()->get('year');
        $countUser = DateTimeHelper::countUserChose($month,$year);
        return $countUser;
    }

    public function store(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');
        $content = $request->get('content');
        $listUserID = $request->get('list_user_id');
        try {
            $this->service->saveLaborCalendar($month,$year,$listUserID,$content);
            flash()->success('Tạo mới thành công.');
            return redirect()->route('admin::labor_calendar.index',['month' => $month,'year' => $year]);
        }catch (\Exception $e)
        {
            Log::info($e->getMessage());
            flash()->info('Tạo mới thất bại.');
            return redirect()->route('admin::labor_calendar.index');
        }
    }

    public function edit($laborCalendarID)
    {
        $laborCalendar = $this->service->getInfoLaborCalendar($laborCalendarID);
        return view($this->resourceAlias.'.edit', $this->filterEditViewData($laborCalendar, [
            'record' => $laborCalendar,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
            'addVarsForView' => $this->addVarsEditViewData()
        ]));
    }

    public function update(Request $request, $id)
    {
        $updateInfo = $request->only(['status','note']);
        $updateLabor = $this->repository->findOne($id);
        $month = Carbon::parse($updateLabor->labor_date)->month;
        $year = Carbon::parse($updateLabor->labor_date)->year;

        try {
            $this->repository->update($updateLabor,$updateInfo);
            flash()->success('Cập nhật thành công.');
            return redirect()->route('admin::labor_calendar.index',['month' => $month, 'year' => $year]);
        }catch (\Exception $e)
        {
            Log::info($e->getMessage());
            flash()->info('Cập nhật thất bại.');
            return redirect()->route('admin::labor_calendar.index');
        }
    }

}
