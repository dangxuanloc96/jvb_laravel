<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OTListExport;
use App\Models\OverTime;
use App\Repositories\Contracts\IOverTimeRepository;
use App\Services\Contracts\IOverTimeService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OverTimeController extends AdminBaseController
{
    //
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.over_times';
    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::over_times';
    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = OverTime::class;
    protected $resourceSearchExtend = 'admin.over_times._search_extend';
    /**
     * @var IOverTimeService
     */
    private $service;

    /**
     * Controller construct
     *
     * @param IOverTimeRepository $repository
     * @param IOverTimeService    $service
     */
    public function __construct(IOverTimeRepository $repository, IOverTimeService $service)
    {
        $this->repository = $repository;
        parent::__construct();
        $this->service = $service;
    }

    /**
     * @var  string
     */
    protected $resourceTitle = 'Overtime';

    public function exportData(Request $request, $search = null)
    {
        switch ($request->path()) {
            case 'admin/over_times':
                $overTimes = $this->service->export($request);
                return Excel::download(new OTListExport ($overTimes), "over-time.xlsx");
                break;
            default:
                abort(404);
        }
    }

    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        if (!$request->has('year'))
            $request->merge(['year' => date('Y')]);
        if (!$request->has('month'))
            $request->merge(['month' => date('n')]);

        return $this->service->search($request, $perPage, $search);
    }

}
