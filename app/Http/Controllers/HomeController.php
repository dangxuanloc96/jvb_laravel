<?php

namespace App\Http\Controllers;

use App\Helpers\NotificationHelper;
use App\Models\Event;
use App\Models\LaborCalendar;
use App\Models\LaborCalendarDetail;
use App\Models\Post;
use App\Models\Project;
use App\Models\Punishes;
use App\Models\User;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $posts = Post::select('id', 'name', 'introduction', 'image_url')
            ->where('status', ACTIVE_STATUS)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        $events = Event::select('id', 'name', 'place', 'event_date', 'event_end_date', 'introduction', 'image_url', 'content', 'created_at', 'deadline_at')
            ->where('status', ACTIVE_STATUS)
//            ->whereDate('event_date', '>=', date(DATE_FORMAT))
            ->orderBy('event_date', 'desc')
            ->take(3)->get();
        $projects = Project::select('id', 'name', 'technical', 'image_url')
            ->where('status', ACTIVE_STATUS)
            ->orderBy('id', 'desc')
            ->take(10)
            ->get();

        $dayLaborCalendar = LaborCalendar::where('labor_date',Carbon::now()->toDateString())->first();
        $laborUserName = '';
        if(!empty($dayLaborCalendar))
        {
            foreach ($dayLaborCalendar->laborCalendarDetail as $detail)
            {
                $laborUserName .= User::find($detail['user_id'])->name .' - ';
            }
            $laborUserName = substr($laborUserName,0,strlen($laborUserName) -2);
        }



        return view('end_user.home', compact('posts', 'events', 'projects','laborUserName'));
    }


}
