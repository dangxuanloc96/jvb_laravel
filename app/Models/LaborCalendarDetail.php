<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaborCalendarDetail extends Model
{
    protected $table = 'labor_calendar_details';
    protected $fillable =
        [
            'user_id',
            'labor_calendar_id',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
}
