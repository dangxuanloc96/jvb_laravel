<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReminderMember extends Model
{
	/**
	 * [$table reminder_members]
	 * @var string
	 */
    protected $table = 'reminder_members';

    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
    	'reminder_id',
    	'user_id',
    	'created_at',
    	'updated_at',
    ];
}
