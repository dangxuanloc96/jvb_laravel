<?php
/**
 * WorkTimeModel class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;

class WorkTime extends Model
{
    use FillableFields, OrderableTrait, SearchLikeTrait;

    protected $table = 'work_times';

    protected $fillable = [
        'id',
        'user_id',
        'work_day',
        'start_at',
        'end_at',
        'type',
        'cost',
        'note',
    ];

    const TYPE_NAMES = [
        0 => 'Bình thường',
        1 => 'Đi muộn',
        2 => 'Về sớm',
        4 => 'Overtime',
    ];

    const WORK_TIME_CALENDAR_TYPE = [
        -2 => 'Nghỉ lễ',
        -1 => 'Không chấm công',
        0 => 'Bình thường',
        1 => 'Đi muộn',
        2 => 'Về sớm',
        4 => 'Overtime',
        5 => 'Đi muộn + Overtime',
    ];
    const WORK_TIME_CALENDAR_DISPLAY = [
        -2 => 'Nghỉ lễ',
        -1 => 'Không chấm công',
        0 => '',
        1 => 'Đi muộn',
        2 => 'Về sớm',
        4 => 'Overtime',
        5 => 'Đi muộn + Overtime',
        6 => 'check in',
        7 => 'check out'
    ];

    const TYPES = [
        'calendar_off' => -2,
        'off' => -1,
        'normal' => 0,
        'lately' => 1,
        'early' => 2,
        'ot' => 4,
        'lately_ot' => 5,
        'checkIn' => 6,
        'checkOut' => 7
    ];

    public function scopeSearch($query, $searchTerm)
    {
        return $query->where(function ($q) use ($searchTerm) {
            $q->orWhere('work_day', 'like', '%' . $searchTerm . '%')->orWhere('note', 'like', '%' . $searchTerm . '%')
                ->orWhere('users.name', 'like', '%' . $searchTerm . '%')
                ->orWhere('users.id', $searchTerm)
                ->orWhere('users.staff_code', 'like', '%' . $searchTerm . '%');
        })
            ->join('users', 'users.id', 'user_id')
            ->select('work_times.*')
            ->orderBy('work_times.work_day', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);//->where('status', ACTIVE_STATUS);
    }

    public function explanation($work_day)
    {
        return $this->hasOne(WorkTimesExplanation::class, 'user_id', 'user_id')->where('work_times_explanation.work_day', $work_day)->first();
    }

}
