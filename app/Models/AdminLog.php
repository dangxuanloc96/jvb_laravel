<?php
/**
 * AdminLogModel class
 * Author: jvb
 * Date: 2019/06/27 16:45
 */

namespace App\Models;

class AdminLog extends Model
{
    protected $table = 'admin_logs';

    protected $fillable = [
        'id',
        'user_id',
        'path',
        'method',
        'json_log',
        'created_at',
        'updated_at',
    ];
}
