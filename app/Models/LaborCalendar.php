<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaborCalendar extends Model
{
    protected $table = 'labor_calendars';
    protected $fillable =
        [
            'creator_id',
            'content',
            'note',
            'status',
            'labor_date',
            'created_at',
            'updated_at',
            'deleted_at',
        ];

    public function laborCalendarDetail()
    {
        return $this->hasMany('App\Models\LaborCalendarDetail','labor_calendar_id');
    }

}
