<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Reminder extends Model
{
    use Notifiable, SoftDeletes;
	/**
	 * [$table reminders]
	 * @var string
	 */
    protected $table = 'reminders';

    protected $fillable = [
    	'name',
    	'content',
    	'start_date',
    	'status',
    	'option_loop',
    	'start_loop',
    	'end_loop',
        'last_notice',
        'route_detail_reminder',
    	'meeting_id',
        'meeting_deleted',
        'delete_at',
    	'created_at',
    	'updated_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'reminder_members', 'reminder_id', 'user_id');
    }

    public function countUsers()
    {
        return User::withCount('id')->get();
    }
}
