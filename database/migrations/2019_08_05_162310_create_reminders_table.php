<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500);
            $table->text('content');
            $table->timestamp('start_date');
            $table->tinyInteger('status')->default(0)->comment('Trạng thái công việc: 1-hoàn thành, 0-chưa hoàn thành');
            $table->tinyInteger('option_loop')->default(0)->comment('Lặp lại nhắc việc theo thời gian định sẵn');
            $table->time('start_loop')->comment('Thời gian bắt đầu lặp');
            $table->time('end_loop')->comment('Thời gian kết thúc lặp');
            $table->time('last_notice')->comment('Thời gian cron');
            $table->time('route_detail_reminder')->comment('url detail reminder');
            $table->integer('meeting_id')->comment('id lịch họp');
            $table->tinyInteger('meeting_deleted')->default(0)->comment('1-đã xoá lịch họp, 0-chưa xoá lịch họp');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders');
    }
}
