<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportFieldReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configs', function (Blueprint $table) {
            //
            $table->tinyInteger('enable_weekly_report_check')->nullable();
            $table->string('weekly_report_check_day', 20)->nullable();
            $table->time('weekly_report_check_time')->nullable();
            $table->tinyInteger('must_comfirm_weekly_report_check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
