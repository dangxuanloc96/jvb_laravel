<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year');
            $table->integer('week_num');
            $table->integer('user_id');
            $table->string('to_ids');
            $table->string('title');
            $table->longText('content');
            $table->tinyInteger('status')->default(0)->comment('0: Nháp; 1: Lưu');

            $table->timestamps();
            $table->softDeletes();

            $table->index(['week_num', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
