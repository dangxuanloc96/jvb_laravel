<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $list = [];
        for($i = 0; $i < 100 ; $i++)
        {
            $user = [];
            $user ['staff_code'] = $faker->postcode;
            $user ['name'] = $faker->name;
            $user ['phone'] = $faker->phoneNumber;
            $user ['email'] = $faker->email;
            $user ['password'] = $faker->password;
            $user ['contract_type'] = rand(0,3);
            $list [] = $user;
        }

        DB::table('users')->insert($list);
    }
}
